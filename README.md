# Build
First of all, make sure the SCAR library is already installed and visible in the CMAKE_PREFIX_PATH

```sh
source /opt/ros/dashing/setup.bash
colcon build --packages-up-to scar_rigid_body
```

# Run
To run, the node needs to link with libraries from the SCAR project. If these libraries are not in the
LD_LIBRARY_PATH, add is using
```sh
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/path/to/the/scar_lib
```
and make sure to source the ROS2 environment. Then, just run one of the nodes, such as
```sh
scar_rbd_control
```

This node inherits from the ROS2 LifecycleNode class. To configure the node, change its state with
```sh
ros2 service call /scar_rbd_control/change_state lifecycle_msgs/srv/ChangeState "{transition: {id: 1}}"
```
and then activate the node with
```sh
ros2 service call /scar_rbd_control/change_state lifecycle_msgs/srv/ChangeState "{transition: {id: 3}}"
```

At any time you can check the available transition ids with
```sh
ros2 lifecycle list /scar_rbd_control
```

# License
This Software is distributed under the [MIT License](https://opensource.org/licenses/MIT).
