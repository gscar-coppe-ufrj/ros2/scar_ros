import os
import time
import unittest

from math import sin, pi

import launch
import launch_ros
import launch_ros.actions
import launch_testing.util
import launch_testing_ros
import rclpy
import rclpy.context
import rclpy.executors
import geometry_msgs.msg
import nav_msgs.msg
import builtin_interfaces

from rclpy.qos import qos_profile_sensor_data

import os.path

def generate_test_description(ready_fn):
    # Necessary to get real-time stdout from python processes:
    proc_env = os.environ.copy()
    proc_env['PYTHONUNBUFFERED'] = '1'

    parameters_file = os.path.join(os.path.dirname(__file__), 'network.test.yaml')
    network = launch_ros.actions.Node(
        package='scar_ros',
        node_executable='test_rbd_control_network',
        env=proc_env,
        parameters=[parameters_file]
    )

    return (
        launch.LaunchDescription([
            network,
            # Start tests right away - no need to wait for anything
            launch.actions.OpaqueFunction(function=lambda context: ready_fn())
        ]),
        {
            'network': network,
        }
    )

def get_time_from_secs(secs):
    time = builtin_interfaces.msg._time.Time()
    time.sec = int(secs)
    time.nanosec = int(1e9 * (secs - time.sec))
    return time

class DataReader:
    def __init__(self, msg, verbose = True):
        self.__msg = msg
        self.__ready = False
        self.__count = 0
        self.verbose = verbose

    def read(self, msg):
        if (self.__ready and self.verbose):
            print("Throwing message away")
        self.__msg = msg
        self.__ready = True
        self.__count += 1

    def process(self):
        ret = self.__ready
        self.__ready = False
        return (self.__msg, ret)

    def count(self):
        return self.__count

    @classmethod
    def convert_time(cls, msg_time):
        return msg_time.sec + msg_time.nanosec / 1000000000.

class TestNetwork(unittest.TestCase):

    reader = []

    @classmethod
    def setUpClass(cls, proc_output, network):
        cls.context = rclpy.context.Context()
        rclpy.init(context=cls.context)
        cls.node = rclpy.create_node('test_node', context=cls.context)

        TestNetwork.reader = DataReader(nav_msgs.msg.Odometry(), False)
        reader = TestNetwork.reader
        subscriber = cls.node.create_subscription(
            nav_msgs.msg.Odometry,
            'odom',
            lambda msg: reader.read(msg),
            qos_profile_sensor_data
        )
        executor = rclpy.executors.SingleThreadedExecutor(context=cls.context)
        executor.add_node(cls.node)
        recv = False
        for _ in range(50):
            try:
                end_time = time.time() + 1
                recv = False
                while (time.time() < end_time and not recv):
                    executor.spin_once(timeout_sec=1.0)
                    (_, recv) = reader.process()
                if (recv):
                    break;
            except:
                break
        executor.remove_node(cls.node)
        if not recv:
            assert False, 'Failed to plumb chatter topic to listener process'
        TestNetwork.reader = reader

    @classmethod
    def tearDownClass(cls):
        cls.node.destroy_node()

    def spin_rclpy(self, timeout_sec):
        executor = rclpy.executors.SingleThreadedExecutor(context=self.context)
        executor.add_node(self.node)
        try:
            executor.spin_once(timeout_sec=timeout_sec)
        finally:
            executor.remove_node(self.node)

    def test_step(self, network):
        reader = TestNetwork.reader
        subscriber = self.node.create_subscription(
            nav_msgs.msg.Odometry,
            'odom',
            lambda msg: reader.read(msg),
            qos_profile_sensor_data
        )
        self.addCleanup(self.node.destroy_subscription, subscriber)
        overshoot = 1.28
        msg_time = DataReader.convert_time(reader.process()[0].header.stamp)
        timeout = msg_time + 3
        while (msg_time < timeout):
            end_time = time.time() + 1
            recv = False
            while (time.time() < end_time and not recv):
                self.spin_rclpy(1.0)
                (msg_odom, recv) = reader.process()

            # Assert that all elements are equal
            odom_pos = [msg_odom.pose.pose.position.x, msg_odom.pose.pose.position.y, msg_odom.pose.pose.position.z]
            self.assertTrue(odom_pos.count(odom_pos[0]) == len(odom_pos))

            # Assert maximum value
            self.assertTrue(odom_pos[0] < overshoot)

            # Update time
            msg_time = DataReader.convert_time(msg_odom.header.stamp)
