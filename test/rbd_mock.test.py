import os
import time
import unittest

from math import sin, pi

import launch
import launch_ros
import launch_ros.actions
import launch_testing.util
import launch_testing_ros
import rclpy
import rclpy.context
import rclpy.executors
import geometry_msgs.msg
import nav_msgs.msg
import builtin_interfaces

from rclpy.qos import qos_profile_sensor_data

def generate_test_description(ready_fn):
    # Necessary to get real-time stdout from python processes:
    proc_env = os.environ.copy()
    proc_env['PYTHONUNBUFFERED'] = '1'

    mock_node = launch_ros.actions.Node(
        package='scar_ros',
        node_executable='rbd_mock_node',
        env=proc_env
    )

    return (
        launch.LaunchDescription([
            mock_node,
            # Start tests right away - no need to wait for anything
            launch.actions.OpaqueFunction(function=lambda context: ready_fn()),
        ]),
        {
            'mock': mock_node,
        }
    )

def get_time_from_secs(secs):
    time = builtin_interfaces.msg._time.Time()
    time.sec = int(secs)
    time.nanosec = int(1e9 * (secs - time.sec))
    return time

class DataReader:
    def __init__(self, msg, verbose = True):
        self.__msg = msg
        self.__ready = False
        self.__count = 0
        self.verbose = verbose

    def read(self, msg):
        if (self.__ready and self.verbose):
            print("Throwing message away")
        self.__msg = msg
        self.__ready = True
        self.__count += 1

    def process(self):
        ret = self.__ready
        self.__ready = False
        return (self.__msg, ret)

    def count(self):
        return self.__count

    @classmethod
    def convert_time(cls, msg_time):
        return msg_time.sec + msg_time.nanosec / 1000000000.

class TestMock(unittest.TestCase):

    reader = []
    time_map = ()

    @classmethod
    def setUpClass(cls, proc_output, mock):
        cls.context = rclpy.context.Context()
        rclpy.init(context=cls.context)
        cls.node = rclpy.create_node('test_node', context=cls.context)

        # The demo node listener has no synchronization to indicate when it's ready to start
        # receiving messages on the /chatter topic.  This plumb_listener method will attempt
        # to publish for a few seconds until it sees output
        publisher = cls.node.create_publisher(
            geometry_msgs.msg.Wrench,
            'cmd',
            10
        )
        TestMock.reader = DataReader(nav_msgs.msg.Odometry(), False)
        reader = TestMock.reader
        subscriber = cls.node.create_subscription(
            nav_msgs.msg.Odometry,
            'odom',
            lambda msg: reader.read(msg),
            qos_profile_sensor_data
        )
        executor = rclpy.executors.SingleThreadedExecutor(context=cls.context)
        executor.add_node(cls.node)
        msg = geometry_msgs.msg.Wrench()
        recv = False
        for _ in range(5):
            try:
                publisher.publish(msg)
                end_time = time.time() + 1
                recv = False
                while (time.time() < end_time and not recv):
                    executor.spin_once(timeout_sec=1.0)
                    (_, recv) = reader.process()
                if (recv):
                    break;
            except:
                break
        executor.remove_node(cls.node)
        if not recv:
            assert False, 'Failed to plumb chatter topic to listener process'

    @classmethod
    def tearDownClass(cls):
        cls.node.destroy_node()

    def spin_rclpy(self, timeout_sec):
        executor = rclpy.executors.SingleThreadedExecutor(context=self.context)
        executor.add_node(self.node)
        try:
            executor.spin_once(timeout_sec=timeout_sec)
        finally:
            executor.remove_node(self.node)

    def test_step(self, mock):
        publisher = self.node.create_publisher(
            geometry_msgs.msg.Wrench,
            'cmd',
            10
        )
        reader = TestMock.reader
        subscriber = self.node.create_subscription(
            nav_msgs.msg.Odometry,
            'odom',
            lambda msg: reader.read(msg),
            qos_profile_sensor_data
        )
        self.addCleanup(
            lambda: (
                (self.node.destroy_publisher(publisher)),
                (self.node.destroy_subscription(subscriber))
            )
        )
        msg = geometry_msgs.msg.Wrench()
        msg_time = DataReader.convert_time(reader.process()[0].header.stamp)
        timeout = msg_time + 3
        amplitude = 1.
        msg.force = geometry_msgs.msg.Vector3(
            x = amplitude,
            y = amplitude,
            z = amplitude,
        )
        start_time = msg_time
        while (msg_time < timeout):
            publisher.publish(msg)
            end_time = time.time() + 1
            recv = False
            while (time.time() < end_time and not recv):
                self.spin_rclpy(1.0)
                (msg_odom, recv) = reader.process()

            # Assert that all elements are equal
            odom_pos = [msg_odom.pose.pose.position.x, msg_odom.pose.pose.position.y, msg_odom.pose.pose.position.z]
            self.assertTrue(odom_pos.count(odom_pos[0]) == len(odom_pos))

            # Update time
            msg_time = DataReader.convert_time(msg_odom.header.stamp)

            # Assert value
            self.assertAlmostEqual(odom_pos[0], amplitude * (msg_time - start_time)**2 / 2, places = 1)
