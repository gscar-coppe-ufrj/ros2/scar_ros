#pragma once


#include <rclcpp/node.hpp>
#include <rclcpp/subscription.hpp>
#include <rclcpp/publisher.hpp>
#include <geometry_msgs/msg/wrench.hpp>
#include <geometry_msgs/msg/accel_stamped.hpp>
#include <geometry_msgs/msg/accel_with_covariance_stamped.hpp>
#include <nav_msgs/msg/odometry.hpp>
#include <message_filters/sync_policies/latest_stamped.h>
#include <message_filters/subscriber.h>

#include <scar/linear-system/LinearSystem.hpp>

#include <control_network/node.hpp>


namespace scar_ros
{


/**
 * @brief Node to mock a rigid body (attitude not implemented)
 */
class RigidBodyMock : public rclcpp::Node, public control_network::Node
{
private:
    rclcpp::Subscription<geometry_msgs::msg::Wrench>::SharedPtr sub_wrench;
    rclcpp::Publisher<nav_msgs::msg::Odometry>::SharedPtr pub_odom;
    rclcpp::Publisher<geometry_msgs::msg::AccelStamped>::SharedPtr pub_accel;
    rclcpp::Publisher<geometry_msgs::msg::AccelWithCovarianceStamped>::SharedPtr pub_accel_cov;

    geometry_msgs::msg::Wrench::SharedPtr msg_wrench;

    scar::linear_system::LinearSystem sys_pos, sys_vel;

    double mass;
    bool flag_first;

    void onWrenchReceived(const geometry_msgs::msg::Wrench::SharedPtr msg);

public:
    RigidBodyMock(double mass, bool publish_covariance = false, bool intra_process_comms = false);


// control_network::Node interface
public:
    bool update(const rclcpp::Time& time) override;
};


}
