#pragma once

#include "scar_ros/dummy_trajectory.hpp"
#include <control_network/node.hpp>

#include <rclcpp/node.hpp>
#include <rclcpp/qos.hpp>
#include <rclcpp/publisher.hpp>
#include <nav_msgs/msg/odometry.hpp>
#include <geometry_msgs/msg/accel_stamped.hpp>

#include <cmath>


namespace scar_ros
{


/**
 * The DummyTrajectory implements a sine reference signal with prescribed amplitude and period
 * in position (x, y, z). The attitude is left at identity.
 */
class DummyTrajectory : public rclcpp::Node, public control_network::Node
{
private:
    double amplitude;
    double frequency;
    double phase;

    rclcpp::Time time_start;
    bool flag_first;

    rclcpp::Publisher<nav_msgs::msg::Odometry>::SharedPtr pub_odom;
    rclcpp::Publisher<geometry_msgs::msg::AccelStamped>::SharedPtr pub_accel;

public:
    DummyTrajectory(double amplitude, double period, double phase = 0, bool intra_process_comms = false);

// control_network::Node interface
public:
    bool update(const rclcpp::Time& time) override;
};


}
