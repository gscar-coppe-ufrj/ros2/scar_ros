#include "scar_ros/dummy_trajectory.hpp"


using namespace ::scar_ros;


DummyTrajectory::DummyTrajectory(double amplitude, double period, double phase, bool intra_process_comms) :
    rclcpp::Node("scar_dummy_trajectory", rclcpp::NodeOptions().use_intra_process_comms(intra_process_comms)),
    amplitude(amplitude),
    frequency(2*M_PI/period),
    phase(phase),
    flag_first(true)
{
    using namespace std::placeholders;

    auto qos = rclcpp::QoS(rclcpp::KeepLast(5)).durability_volatile().best_effort();
    pub_odom  = create_publisher<nav_msgs::msg::Odometry>("ref", qos);
    pub_accel = create_publisher<geometry_msgs::msg::AccelStamped>("ref_ff", qos);
}

bool DummyTrajectory::update(const rclcpp::Time& time)
{
    if (flag_first)
    {
        time_start = time;
        flag_first = false;
    }
    double pos, vel, acc;
    auto delta = time - time_start;
    pos = amplitude * std::sin(frequency * delta.seconds() + phase);
    vel = frequency * amplitude * std::cos(frequency * delta.seconds() + phase);
    acc = - frequency * pos;
    //
    nav_msgs::msg::Odometry msg_odom;
    msg_odom.header.set__stamp(time);
    msg_odom.pose.pose.position.set__x(pos).set__y(pos).set__z(pos);
    msg_odom.twist.twist.linear.set__x(vel).set__y(vel).set__z(vel);
    msg_odom.pose.pose.orientation.set__w(1);
    //
    geometry_msgs::msg::AccelStamped msg_accel;
    msg_accel.header.set__stamp(time);
    msg_accel.accel.linear.set__x(acc).set__y(acc).set__z(acc);
    //
    pub_odom->publish(msg_odom);
    pub_accel->publish(msg_accel);
    return true;
}
