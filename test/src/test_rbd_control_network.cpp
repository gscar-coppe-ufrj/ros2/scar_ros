#include "scar_ros/rigid_body_mock.hpp"
#include "scar_ros/dummy_trajectory.hpp"
#include "scar_ros/rigid_body_control.hpp"
#include "scar_ros/conversions/time.hpp"

#include <rclcpp/executors/single_threaded_executor.hpp>
#include <lifecycle_msgs/msg/transition.hpp>

#include <scar/types/Time.hpp>


using namespace ::scar_ros;

typedef std::tuple<
    rclcpp::node_interfaces::NodeBaseInterface::SharedPtr,
    control_network::Node::SharedPtr
> ExecutorNode;

typedef std::deque<ExecutorNode> NodeVector;


void spin(rclcpp::executors::SingleThreadedExecutor& exe, NodeVector& nodes, const rclcpp::Time& time)
{
    for (auto iter = nodes.begin(); iter != nodes.end(); ++iter)
    {
        auto node_interface = std::get<0>(*iter);
        auto node_control_network = std::get<1>(*iter);
        exe.spin_node_some(node_interface);
        if (node_control_network)
            node_control_network->update(time);
    }
}

template <class NodePtr>
void add(NodeVector& nodes, NodePtr node)
{
    nodes.push_back(
        std::make_tuple(
            node->get_node_base_interface(),
            std::dynamic_pointer_cast<control_network::Node>(node)
        )
    );
}

void activate(NodeVector& nodes)
{
    using lifecycle_msgs::msg::Transition;
    for (auto iter = nodes.begin(); iter != nodes.end(); ++iter)
    {
        auto node_control_network = std::get<1>(*iter);
        auto node_lifecycle = std::dynamic_pointer_cast<rclcpp_lifecycle::LifecycleNode>(node_control_network);
        if (node_lifecycle)
        {
            node_lifecycle->trigger_transition(Transition::TRANSITION_CONFIGURE);
            node_lifecycle->trigger_transition(Transition::TRANSITION_ACTIVATE);
        }
    }
}

int main(int argc, char** argv)
{
    using scar::types::Time;

    rclcpp::init(argc, argv);
    rclcpp::executors::SingleThreadedExecutor exe;
    std::shared_ptr<rclcpp::Node> node_mock = std::make_shared<RigidBodyMock>(1);
    std::shared_ptr<rclcpp::Node> node_traj = std::make_shared<DummyTrajectory>(1, std::numeric_limits<double>::max(), M_PI/2);
    std::shared_ptr<rclcpp_lifecycle::LifecycleNode> node_ctrl = std::make_shared<RigidBodyControl>();

    NodeVector nodes;
    add(nodes, node_traj);
    add(nodes, node_mock);
    add(nodes, node_ctrl);
    activate(nodes);

    rclcpp::Duration duration(0, 5000000);
    rclcpp::Time last = node_mock->now();
    while (rclcpp::ok())
    {
        spin(exe, nodes, last);

        rclcpp::Duration elapsed = node_mock->now() - last;
        if (duration > elapsed)
            rclcpp::sleep_for((duration - elapsed).to_chrono<std::chrono::nanoseconds>());
        last = node_mock->now();
    }

    rclcpp::shutdown();
    return 0;
}
