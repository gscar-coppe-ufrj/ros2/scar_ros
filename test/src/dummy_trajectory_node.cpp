#include "scar_ros/conversions/time.hpp"
#include "scar_ros/dummy_trajectory.hpp"

#include <rclcpp/executors/single_threaded_executor.hpp>


using namespace ::scar_ros;


int main(int argc, char** argv)
{
    using scar::types::Time;

    rclcpp::init(argc, argv);
    rclcpp::executors::SingleThreadedExecutor exe;
    std::shared_ptr<rclcpp::Node> node = std::make_shared<DummyTrajectory>(1, 200000000, M_PI / 2);

    rclcpp::Duration duration(0, 5000000);
    rclcpp::Time last = node->now();
    while (rclcpp::ok())
    {
        exe.spin_node_some(node->get_node_base_interface());
        auto control_network_node = std::dynamic_pointer_cast<control_network::Node>(node);
        if (control_network_node)
            control_network_node->update(last);

        rclcpp::Duration elapsed = node->now() - last;
        if (duration > elapsed)
            rclcpp::sleep_for((duration - elapsed).to_chrono<std::chrono::nanoseconds>());
        last = node->now();
    }

    rclcpp::shutdown();
    return 0;
}
