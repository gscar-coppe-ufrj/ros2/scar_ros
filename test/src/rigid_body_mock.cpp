#include "scar_ros/rigid_body_mock.hpp"
#include "scar_ros/conversions/time.hpp"


using namespace ::scar_ros;


RigidBodyMock::RigidBodyMock(double mass, bool publish_covariance, bool intra_process_comms) :
    rclcpp::Node("scar_rbd_mock", rclcpp::NodeOptions().use_intra_process_comms(intra_process_comms)),
    mass(mass),
    flag_first(true)
{
    using namespace std::placeholders;

    auto qos = rclcpp::QoS(rclcpp::KeepLast(5)).durability_volatile().best_effort();

    sub_wrench = create_subscription<geometry_msgs::msg::Wrench>(
          "cmd", qos, std::bind(&RigidBodyMock::onWrenchReceived, this, _1));

    pub_odom  = create_publisher<nav_msgs::msg::Odometry>("odom", qos);
    if (publish_covariance)
        pub_accel_cov = create_publisher<geometry_msgs::msg::AccelWithCovarianceStamped>("accel", qos);
    else
        pub_accel = create_publisher<geometry_msgs::msg::AccelStamped>("accel", qos);

    scar::types::Poly num(1), den(2);
    num << 1;
    den << 1, 0;
    sys_pos = scar::linear_system::LinearSystem(num, den, scar::types::Time::fromMilliseconds(10));
    sys_vel = scar::linear_system::LinearSystem(num, den, scar::types::Time::fromMilliseconds(10));
    sys_pos.useNFilters(3);
    sys_vel.useNFilters(3);
    auto u0 = Eigen::MatrixXd::Zero(3, 1);
    auto ydy0 = Eigen::MatrixXd::Zero(3, 1);
    sys_pos.setInitialConditions(u0, ydy0);
    sys_vel.setInitialConditions(u0, ydy0);

    msg_wrench = std::make_shared<geometry_msgs::msg::Wrench>();
}

bool RigidBodyMock::update(const rclcpp::Time& time)
{
    if (!msg_wrench)
        return false;
    using scar::linear_system::LinearSystem;

    auto time_scar = conversions::convert(time);
    if (flag_first)
    {
        sys_pos.setInitialTime(time_scar);
        sys_vel.setInitialTime(time_scar);
        flag_first = false;
    }
    scar::linear_system::Input input(3);
    input << msg_wrench->force.x, msg_wrench->force.y, msg_wrench->force.z;
    input = input / mass;
    sys_vel.update(input / mass, time_scar);
    auto out_vel = sys_vel.getOutput();
    sys_pos.update(out_vel, time_scar);
    auto out_pos = sys_pos.getOutput();

    std_msgs::msg::Header header;
    header.set__stamp(time);

    nav_msgs::msg::Odometry msg_odom;
    msg_odom.set__header(header);
    msg_odom.header.set__stamp(time);
    msg_odom.pose.pose.position.set__x(out_pos[0]);
    msg_odom.pose.pose.position.set__y(out_pos[1]);
    msg_odom.pose.pose.position.set__z(out_pos[2]);
    msg_odom.twist.twist.linear.set__x(out_vel[0]);
    msg_odom.twist.twist.linear.set__y(out_vel[1]);
    msg_odom.twist.twist.linear.set__z(out_vel[2]);
    pub_odom->publish(msg_odom);

    geometry_msgs::msg::AccelStamped msg_accel;
    msg_accel.set__header(header);
    msg_accel.header.set__stamp(time);
    msg_accel.accel.linear.set__x(input[0]);
    msg_accel.accel.linear.set__y(input[1]);
    msg_accel.accel.linear.set__z(input[2]);
    if (pub_accel)
        pub_accel->publish(msg_accel);
    else if (pub_accel_cov)
    {
        geometry_msgs::msg::AccelWithCovarianceStamped msg_accel_cov;
        msg_accel_cov.accel.set__accel(msg_accel.accel);
        msg_accel_cov.set__header(msg_accel_cov.header);
        pub_accel_cov->publish(msg_accel_cov);
    }
    return true;
}

void RigidBodyMock::onWrenchReceived(const geometry_msgs::msg::Wrench::SharedPtr msg)
{
    msg_wrench = msg;
}
