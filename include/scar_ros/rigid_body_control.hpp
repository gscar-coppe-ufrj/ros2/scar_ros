#pragma once


#include <rclcpp_lifecycle/lifecycle_node.hpp>
#include <rclcpp_lifecycle/lifecycle_publisher.hpp>
#include <geometry_msgs/msg/twist_stamped.hpp>
#include <geometry_msgs/msg/wrench.hpp>
#include <geometry_msgs/msg/accel_with_covariance_stamped.hpp>
#include <geometry_msgs/msg/accel_stamped.hpp>
#include <nav_msgs/msg/odometry.hpp>
#include <message_filters/sync_policies/latest_stamped.h>
#include <message_filters/subscriber.h>

#include <control_network/node.hpp>

#include <scar/controller/PID.hpp>
#include <scar/rbd/RigidBody.hpp>


// Forward declare stuff to avoid too many recompilations
namespace scar_ros
{
namespace control
{

template <typename Node>
class PIDParameters;

}
}


namespace scar_ros
{


class RigidBodyControl : public rclcpp_lifecycle::LifecycleNode, public control_network::Node
{
private:
    typedef message_filters::sync_policies::LatestStamped<
        nav_msgs::msg::Odometry,
        geometry_msgs::msg::AccelStamped,
        nav_msgs::msg::Odometry,
        geometry_msgs::msg::AccelWithCovarianceStamped
    > SyncPolicy;
    typedef std::shared_ptr<SyncPolicy::Sync> SyncPtr;
    SyncPtr sync;

    scar::controller::PID pid;
    scar::rbd::RigidBody body;
    scar::controller::Input ref_pose, ref_twist, ref_ff, pose, twist;
    scar::controller::Output accel;

    // QoS configuration for sensor and control messages
    rclcpp::QoS qos_ctrl;

    message_filters::Subscriber<nav_msgs::msg::Odometry, RigidBodyControl> sub_ref;
    message_filters::Subscriber<geometry_msgs::msg::AccelStamped, RigidBodyControl> sub_ff;
    message_filters::Subscriber<nav_msgs::msg::Odometry, RigidBodyControl> sub_odom;
    message_filters::Subscriber<geometry_msgs::msg::AccelWithCovarianceStamped, RigidBodyControl> sub_accel;

    rclcpp_lifecycle::LifecyclePublisher<geometry_msgs::msg::Wrench>::SharedPtr pub_cmd;
    rclcpp_lifecycle::LifecyclePublisher<nav_msgs::msg::Odometry>::SharedPtr pub_error;

    rclcpp::Parameter param_mode;
    std::shared_ptr<::scar_ros::control::PIDParameters<rclcpp_lifecycle::LifecycleNode>> params_pid;
    rclcpp::Parameter param_sdf_file;
    rclcpp::Parameter param_sdf_string;
    rclcpp::Parameter param_sdf_link;

    /// Indicates if any of the received messages was null
    bool has_null;

    /// True if we are controlling position; false for velocity
    bool is_mode_pos;

private:
    using rclcpp_lifecycle::LifecycleNode::declare_parameter;
    template <typename Type>
    auto declare_parameter(const rclcpp::Parameter& param)
    {
        return declare_parameter(
            param.get_name(),
            param.get_value<Type>()
        );
    }

    using rclcpp_lifecycle::LifecycleNode::get_parameter;
    inline auto get_parameter(rclcpp::Parameter& param)
    {
        return get_parameter(
            param.get_name(),
            param
        );
    }

public:
    explicit RigidBodyControl(bool intra_process_comms = false);

    void onDataReceived(
        const nav_msgs::msg::Odometry::ConstSharedPtr& msg_ref,
        const geometry_msgs::msg::AccelStamped::ConstSharedPtr& msg_ff,
        const nav_msgs::msg::Odometry::ConstSharedPtr& msg_odom,
        const geometry_msgs::msg::AccelWithCovarianceStamped::ConstSharedPtr& msg_accel
    );

    void resetPublishers();

// rclcpp_lifecycle::LifecycleNode interface
public:
    CallbackReturn on_configure(const rclcpp_lifecycle::State& previous_state) override;
    CallbackReturn on_cleanup(const rclcpp_lifecycle::State& previous_state) override;
    CallbackReturn on_shutdown(const rclcpp_lifecycle::State& previous_state) override;
    CallbackReturn on_activate(const rclcpp_lifecycle::State& previous_state) override;
    CallbackReturn on_deactivate(const rclcpp_lifecycle::State& previous_state) override;

// control_network::Node interface
public:
    bool update(const rclcpp::Time& time) override;
};


}
