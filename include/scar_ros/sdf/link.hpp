#pragma once


#include <sdf/sdf.hh>
#include <eigen3/Eigen/Dense>
#include <rclcpp/logger.hpp>


namespace scar_ros
{
namespace sdf
{


class Link
{
private:
    bool ok_;
    double mass_;
    Eigen::Matrix3d inertia_;

public:
    Link(const ::sdf::SDFPtr& sdf, const std::string& link_name, rclcpp::Logger logger);

    inline bool ok() const
    {
        return ok_;
    }

    inline auto mass() const
    {
        return mass_;
    }

    inline const auto& inertia() const
    {
        return inertia_;
    }
};


}
}
