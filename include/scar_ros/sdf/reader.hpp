#pragma once


#include <sdf/sdf.hh>
#include <rclcpp/logger.hpp>
#include <rclcpp/parameter.hpp>


namespace scar_ros
{
namespace sdf
{


::sdf::SDFPtr read(
    const rclcpp::Parameter& param_sdf_file,
    const rclcpp::Parameter& param_sdf_string,
    rclcpp::Logger logger
);

std::string expandPath(
    const std::string& model_path
);


}
}
