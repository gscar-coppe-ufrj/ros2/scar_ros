#pragma once

#include <scar/types/Time.hpp>
#include <rclcpp/duration.hpp>
#include <rclcpp/time.hpp>
#include <tf2/time.h>


namespace scar_ros
{
namespace conversions
{


inline scar::types::Time convert(const rclcpp::Time& time)
{
    return scar::types::Time::fromMicroseconds(time.nanoseconds() / 1000L);
}

inline scar::types::Time convert(const rclcpp::Duration& time)
{
    return scar::types::Time::fromMicroseconds(time.nanoseconds() / 1000L);
}

inline rclcpp::Time convert(const scar::types::Time& time)
{
    int32_t secs   = static_cast<int32_t>(std::floor(time.toSeconds()));
    uint32_t nsecs = static_cast<uint32_t>((time.toMicroseconds() - secs * 1000000) * 1000);
    return rclcpp::Time(secs, nsecs);
}

inline void convert(const rclcpp::Time& time_from, tf2::TimePoint& time_to)
{
    time_to = tf2::TimePoint( std::chrono::nanoseconds(time_from.nanoseconds()) );
}

}
}
