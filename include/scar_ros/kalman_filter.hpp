#pragma once


#include <rclcpp_lifecycle/lifecycle_node.hpp>
#include <rclcpp_lifecycle/lifecycle_publisher.hpp>
#include <sensor_msgs/msg/nav_sat_fix.hpp>
#include <sensor_msgs/msg/imu.hpp>
#include <nav_msgs/msg/odometry.hpp>
#include <geometry_msgs/msg/accel_with_covariance_stamped.hpp>
#include <message_filters/sync_policies/latest.h>
#include <message_filters/subscriber.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2_ros/transform_listener.h>
#include <tf2_ros/transform_broadcaster.h>

#include <control_network/node.hpp>

#include <scar/linear-system/LinearSystem.hpp>
#include <scar/rbd/FrameRelation.hpp>


namespace scar_ros
{


class KalmanFilter : public rclcpp_lifecycle::LifecycleNode, public control_network::Node
{
private:
    // Message synchronization
    typedef message_filters::sync_policies::Latest<
        sensor_msgs::msg::NavSatFix,
        sensor_msgs::msg::Imu
    > SyncPolicy;
    std::shared_ptr<SyncPolicy::Sync> sync;

    // QoS configuration for sensor and control messages
    rclcpp::QoS qos_ctrl;

    // Subscribers
    message_filters::Subscriber<sensor_msgs::msg::NavSatFix, KalmanFilter> sub_gps;
    message_filters::Subscriber<sensor_msgs::msg::Imu, KalmanFilter> sub_imu;

    // Publishers
    rclcpp_lifecycle::LifecyclePublisher<nav_msgs::msg::Odometry>::SharedPtr pub_odom;
    rclcpp_lifecycle::LifecyclePublisher<geometry_msgs::msg::AccelWithCovarianceStamped>::SharedPtr pub_accel;

    // Messages
    sensor_msgs::msg::NavSatFix msg_gps;
    sensor_msgs::msg::Imu msg_imu;
    std_msgs::msg::Header msg_cmd_header;
    nav_msgs::msg::Odometry msg_odom;
    geometry_msgs::msg::AccelWithCovarianceStamped msg_accel;

    // Parameters
    rclcpp::Parameter param_frame_world, param_frame_odom, param_frame_body, param_frame_gps, param_frame_imu;
    rclcpp::Parameter param_filter_cutoff, param_filter_sampling, param_filter_use;

    // Transform listener
    std::shared_ptr<tf2_ros::TransformListener> tf_listener;
    std::shared_ptr<tf2_ros::TransformBroadcaster> tf_broadcaster;
    tf2_ros::Buffer tf_buffer;

    // Frame relation object to transform data between different frames
    scar::rbd::FrameRelation frame_relation;

    std::shared_ptr<scar::linear_system::LinearSystem> filter_vel;
    bool has_null, first_run;

public:
    explicit KalmanFilter(bool intra_process_comms = false);

    void onDataReceived(
        const sensor_msgs::msg::NavSatFix::ConstSharedPtr& msg_gps,
        const sensor_msgs::msg::Imu::ConstSharedPtr& msg_imu
    );

    void resetPublishers();

// rclcpp_lifecycle::LifecycleNode interface
public:
    CallbackReturn on_configure(const rclcpp_lifecycle::State& previous_state) override;
    CallbackReturn on_cleanup(const rclcpp_lifecycle::State& previous_state) override;
    CallbackReturn on_shutdown(const rclcpp_lifecycle::State& previous_state) override;
    CallbackReturn on_activate(const rclcpp_lifecycle::State& previous_state) override;
    CallbackReturn on_deactivate(const rclcpp_lifecycle::State& previous_state) override;

// control_network::Node interface
public:
    bool update(const rclcpp::Time& time) override;
};


}
