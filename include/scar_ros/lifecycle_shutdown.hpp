#pragma once


#include <rclcpp/logging.hpp>
#include <rclcpp_lifecycle/lifecycle_node.hpp>
#include <lifecycle_msgs/msg/state.hpp>


namespace scar_ros
{


std::shared_ptr<rclcpp_lifecycle::LifecycleNode> global_node;


bool trigger_transition(
    std::shared_ptr<rclcpp_lifecycle::LifecycleNode> node,
    uint8_t state,
    uint8_t desired_state,
    uint8_t transition)
{
    if (node->get_current_state().id() != state)
        return false;
    auto new_state = node->trigger_transition(transition);
    RCLCPP_DEBUG(node->get_logger(), "triggering transition %i to reach state %i",
        transition, desired_state
    );
    if (new_state.id() != desired_state)
        RCLCPP_DEBUG(node->get_logger(), "triggered transition %i but failed to reach state %i",
            transition, desired_state
        );
    return true;
}

void shutdown_handler(int)
{
    if (!global_node)
        return;

    RCLCPP_DEBUG(global_node->get_logger(), "Executing custom signal handle on state ID %i",
        global_node->get_current_state().id());

    if (global_node->get_current_state().id() ==
        lifecycle_msgs::msg::State::PRIMARY_STATE_UNKNOWN) {
            RCLCPP_DEBUG(global_node->get_logger(),
                "Unknown state, therefore there is no transition to trigger"
            );
        }
    else if (trigger_transition(
            global_node,
            lifecycle_msgs::msg::State::PRIMARY_STATE_UNCONFIGURED,
            lifecycle_msgs::msg::State::PRIMARY_STATE_FINALIZED,
            lifecycle_msgs::msg::Transition::TRANSITION_UNCONFIGURED_SHUTDOWN
        )) {}
    else if (trigger_transition(
            global_node,
            lifecycle_msgs::msg::State::PRIMARY_STATE_INACTIVE,
            lifecycle_msgs::msg::State::PRIMARY_STATE_FINALIZED,
            lifecycle_msgs::msg::Transition::TRANSITION_INACTIVE_SHUTDOWN
        )) {}
    else if (trigger_transition(
            global_node,
            lifecycle_msgs::msg::State::PRIMARY_STATE_ACTIVE,
            lifecycle_msgs::msg::State::PRIMARY_STATE_FINALIZED,
            lifecycle_msgs::msg::Transition::TRANSITION_ACTIVE_SHUTDOWN
        )) {}
    else if (global_node->get_current_state().id() ==
        lifecycle_msgs::msg::State::PRIMARY_STATE_FINALIZED)
            RCLCPP_DEBUG(global_node->get_logger(), "Node was already shutdown"
        );

    // This is what is executed on the original SIGINT handle on the
    // SignalHandler::deferred_signal_handler()
    rclcpp::contexts::default_context::get_global_default_context()->shutdown(
        "custom signal handler"
    );
    RCLCPP_DEBUG(global_node->get_logger(), "Passed on shutdown on signal handler");
}


}

