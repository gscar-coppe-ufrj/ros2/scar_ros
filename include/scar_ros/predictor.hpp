#pragma once


#include <rclcpp_lifecycle/lifecycle_node.hpp>
#include <rclcpp_lifecycle/lifecycle_publisher.hpp>
#include <message_filters/subscriber.h>
#include <std_msgs/msg/float64.hpp>
#include <nav_msgs/msg/odometry.hpp>
#include <geometry_msgs/msg/accel_with_covariance_stamped.hpp>
#include <message_filters/sync_policies/latest_stamped.h>
#include <message_filters/subscriber.h>
#include <tf2_ros/transform_listener.h>

#include <control_network/node.hpp>

#include <scar/predictor/Predictor.hpp>
#include <scar/rbd/FrameRelation.hpp>


namespace scar_ros
{


class Predictor: public rclcpp_lifecycle::LifecycleNode, public control_network::Node
{
private:
    // Message synchronization
    typedef message_filters::sync_policies::LatestStamped<
        nav_msgs::msg::Odometry,
        geometry_msgs::msg::AccelWithCovarianceStamped
    > SyncPolicy;
    std::shared_ptr<SyncPolicy::Sync> sync;

    // QoS configuration for sensor and control messages
    rclcpp::QoS qos_ctrl;

    // Subscribers
    message_filters::Subscriber<nav_msgs::msg::Odometry, Predictor> sub_odom;
    message_filters::Subscriber<geometry_msgs::msg::AccelWithCovarianceStamped, Predictor> sub_accel;

    // Publishers
    rclcpp_lifecycle::LifecyclePublisher<nav_msgs::msg::Odometry>::SharedPtr pub_odom;
    rclcpp_lifecycle::LifecyclePublisher<std_msgs::msg::Float64>::SharedPtr pub_quality_pos, pub_quality_rot;

    // Parameters
    rclcpp::Parameter param_frequencies_hz;
    rclcpp::Parameter param_window_memory, param_window_prediction;
    rclcpp::Parameter param_pose_sampling;
    rclcpp::Parameter param_target_frame;

    // Predictor
    scar::predictor::Predictor::UniquePtr predictor_ptr;
    scar::predictor::Waypoints waypoints;
    scar::predictor::Waypoints::iterator waypoints_iter, waypoints_quality_iter;
    scar::types::Time time_last;
    scar::rbd::FrameRelation frame_relation;

    // Transform listener
    std::shared_ptr<tf2_ros::TransformListener> tf_listener;
    tf2_ros::Buffer tf_buffer;

    enum class State : unsigned char;
    State state;
    std::string frame_odom;

private:
    void onDataReceived(
        const nav_msgs::msg::Odometry::ConstSharedPtr& msg_odom,
        const geometry_msgs::msg::AccelWithCovarianceStamped::ConstSharedPtr& msg_accel
    );

public:
    explicit Predictor(bool intra_process_comms = false);

    void resetPublishers();

// rclcpp_lifecycle::LifecycleNode interface
public:
    CallbackReturn on_configure(const rclcpp_lifecycle::State& previous_state) override;
    CallbackReturn on_cleanup(const rclcpp_lifecycle::State& previous_state) override;
    CallbackReturn on_shutdown(const rclcpp_lifecycle::State& previous_state) override;
    CallbackReturn on_activate(const rclcpp_lifecycle::State& previous_state) override;
    CallbackReturn on_deactivate(const rclcpp_lifecycle::State& previous_state) override;

// control_network::Node interface
public:
    bool update(const rclcpp::Time& time) override;
};


}
