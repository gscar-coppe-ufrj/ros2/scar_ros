#pragma once


#include <rclcpp_lifecycle/lifecycle_node.hpp>
#include <rclcpp_lifecycle/lifecycle_publisher.hpp>
#include <message_filters/subscriber.h>
#include <nav_msgs/msg/odometry.hpp>
#include <geometry_msgs/msg/accel_stamped.hpp>
#include <message_filters/sync_policies/latest_stamped.h>
#include <message_filters/subscriber.h>

#include <control_network/node.hpp>

#include <scar/controller/PID.hpp>
#include <scar/rbd/FrameRelation.hpp>


// Forward declare stuff to avoid too many recompilations
namespace scar_ros
{
namespace control
{

template <typename Node>
class PIDParameters;

}
}


namespace scar_ros
{


class SimplePoseControl: public rclcpp_lifecycle::LifecycleNode, public control_network::Node
{
private:
    typedef message_filters::sync_policies::LatestStamped<
        nav_msgs::msg::Odometry,
        nav_msgs::msg::Odometry
    > SyncPolicy;
    typedef std::shared_ptr<SyncPolicy::Sync> SyncPtr;
    SyncPtr sync;

    scar::controller::PID pid;
    scar::rbd::FrameRelation frame_relation;
    scar::controller::Input ref_pose, ref_twist, pose, twist;

    // QoS configuration for sensor and control messages
    rclcpp::QoS qos_ctrl;

    // Subscribers
    message_filters::Subscriber<nav_msgs::msg::Odometry, SimplePoseControl> sub_ref;
    message_filters::Subscriber<nav_msgs::msg::Odometry, SimplePoseControl> sub_odom;

    // Publishers
    rclcpp_lifecycle::LifecyclePublisher<nav_msgs::msg::Odometry>::SharedPtr pub_cmd;
    rclcpp_lifecycle::LifecyclePublisher<nav_msgs::msg::Odometry>::SharedPtr pub_error;
    rclcpp_lifecycle::LifecyclePublisher<geometry_msgs::msg::AccelStamped>::SharedPtr pub_dot_cmd;

    // Parameters
    std::shared_ptr<::scar_ros::control::PIDParameters<rclcpp_lifecycle::LifecycleNode>> params_pid;

    /// Indicates if any of the received messages was null
    bool has_null;

public:
    explicit SimplePoseControl(bool intra_process_comms = false);

    void onDataReceived(
        const nav_msgs::msg::Odometry::ConstSharedPtr& msg_ref,
        const nav_msgs::msg::Odometry::ConstSharedPtr& msg_odom
    );

    void resetPublishers();

// rclcpp_lifecycle::LifecycleNode interface
public:
    CallbackReturn on_configure(const rclcpp_lifecycle::State& previous_state) override;
    CallbackReturn on_cleanup(const rclcpp_lifecycle::State& previous_state) override;
    CallbackReturn on_shutdown(const rclcpp_lifecycle::State& previous_state) override;
    CallbackReturn on_activate(const rclcpp_lifecycle::State& previous_state) override;
    CallbackReturn on_deactivate(const rclcpp_lifecycle::State& previous_state) override;

// control_network::Node interface
public:
    bool update(const rclcpp::Time& time) override;
};


}
