/**
 * Class to control joint angles
 *
 * TODO
 * 1- Implement feedforward for a generic case
 * 2- Generalize dynamic equations to `M*theta_dd + C * theta_d + G = u`
 */
#pragma once


#include <rclcpp_lifecycle/lifecycle_node.hpp>
#include <rclcpp_lifecycle/lifecycle_publisher.hpp>
#include <sensor_msgs/msg/joint_state.hpp>
#include <message_filters/sync_policies/latest_stamped.h>
#include <message_filters/subscriber.h>

#include <control_network/node.hpp>

#include <scar/controller/PID.hpp>
#include <scar/rbd/RigidBody.hpp>


// Forward declare stuff to avoid too many recompilations
namespace scar_ros
{
namespace control
{

template <typename Node>
class PIDParameters;

}
}


namespace scar_ros
{


class GimbalJointControl : public rclcpp_lifecycle::LifecycleNode, public control_network::Node
{
private:
    typedef message_filters::sync_policies::LatestStamped<
        sensor_msgs::msg::JointState,
        sensor_msgs::msg::JointState
    > SyncPolicy;
    typedef std::shared_ptr<SyncPolicy::Sync> SyncPtr;
    SyncPtr sync;
    std::unique_ptr<scar::controller::PID> pid;
    std::vector<scar::rbd::RigidBody> rigid_bodies;
    scar::controller::Input ref_joint_pos, ref_joint_vel, ref_ff, joint_pos, joint_vel;
    scar::controller::Output accel;

    // QoS configuration for sensor and control messages
    rclcpp::QoS qos_ctrl;
    message_filters::Subscriber<sensor_msgs::msg::JointState, GimbalJointControl> sub_joint_state;
    message_filters::Subscriber<sensor_msgs::msg::JointState, GimbalJointControl> sub_joint_state_ref;

    rclcpp_lifecycle::LifecyclePublisher<sensor_msgs::msg::JointState>::SharedPtr pub_cmd;
    rclcpp_lifecycle::LifecyclePublisher<sensor_msgs::msg::JointState>::SharedPtr pub_error;

    std::shared_ptr<::scar_ros::control::PIDParameters<rclcpp_lifecycle::LifecycleNode>> params_pid;
    rclcpp::Parameter param_sdf_file;
    rclcpp::Parameter param_sdf_string;
    rclcpp::Parameter param_sdf_link;
    rclcpp::Parameter param_sdf_joint;

    std::vector<std::string> joint_names;

    /// Indicates if any of the received messages was null
    bool has_null;

private:
    using rclcpp_lifecycle::LifecycleNode::declare_parameter;
    template <typename Type>
    auto declare_parameter(const rclcpp::Parameter& param)
    {
        return declare_parameter(
            param.get_name(),
            param.get_value<Type>()
        );
    }

    using rclcpp_lifecycle::LifecycleNode::get_parameter;
    inline auto get_parameter(rclcpp::Parameter& param)
    {
        return get_parameter(
            param.get_name(),
            param
        );
    }

public:
    explicit GimbalJointControl
(bool intra_process_comms = false);

    void onDataReceived(
        const sensor_msgs::msg::JointState::ConstSharedPtr& msg_joint_state,
        const sensor_msgs::msg::JointState::ConstSharedPtr& msg_joint_state_ref
    );

    void resetPublishers();

// rclcpp_lifecycle::LifecycleNode interface
public:
    CallbackReturn on_configure(const rclcpp_lifecycle::State& previous_state) override;
    CallbackReturn on_cleanup(const rclcpp_lifecycle::State& previous_state) override;
    CallbackReturn on_shutdown(const rclcpp_lifecycle::State& previous_state) override;
    CallbackReturn on_activate(const rclcpp_lifecycle::State& previous_state) override;
    CallbackReturn on_deactivate(const rclcpp_lifecycle::State& previous_state) override;

// control_network::Node interface
public:
    bool update(const rclcpp::Time& time) override;
};


}
