#pragma once


#include <rclcpp/parameter.hpp>
#include <rclcpp/node.hpp>
#include <rclcpp_lifecycle/lifecycle_node.hpp>


namespace scar_ros
{
namespace control
{


template <typename Node>
class ControlParameters
{
private:
    std::vector<rclcpp::Parameter> pool;
    Node* ptr_node;

protected:
    /**
     * @brief Adds a new parameter to the parameter pool
     * @param param Parameter to add
     * @return Returns the parameter index
     */
    inline unsigned int add(rclcpp::Parameter param)
    {
        pool.push_back(param);
        return pool.size() - 1;
    }

    inline const decltype(pool)& getParameterPool() const
    {
        return pool;
    }

public:
    ControlParameters(Node* node, double def_sampling = 0.01, bool def_feedforward = false) :
        ptr_node(node)
    {
        add( rclcpp::Parameter("pid/sampling",           def_sampling) );
        add( rclcpp::Parameter("pid/use_feedforward", def_feedforward) );
    }

    inline void declare()
    {
        for (const auto& param : pool)
            ptr_node->declare_parameter(param.get_name(), param.get_parameter_value());
    }

    inline void read()
    {
        for (auto& param : pool)
            ptr_node->get_parameter(param.get_name(), param);
    }

    inline auto sampling() const
    {
        return pool[0].as_double();
    }

    inline auto feedforward() const
    {
        return pool[1].as_bool();
    }
};

}
}
