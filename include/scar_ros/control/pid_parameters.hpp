#pragma once


#include "scar_ros/control/control_parameters.hpp"


namespace scar_ros
{
namespace control
{


template <typename Node>
class PIDParameters : public ControlParameters<Node>
{
private:
    unsigned int params[5];

public:
    PIDParameters(
        Node* node,
        double def_sampling = 0.01,
        bool def_feedforward = false,
        double def_overshoot = 0.1,
        double def_settling = 20.0,
        bool def_estimate_derivative = true,
        bool def_use_integral = true,
        bool def_use_prefilter = true
    ) : ControlParameters<Node>(node, def_sampling, def_feedforward)
    {
        params[0] = this->add( rclcpp::Parameter("pid/overshoot",           def_overshoot) );
        params[1] = this->add( rclcpp::Parameter("pid/settling",            def_settling) );
        params[2] = this->add( rclcpp::Parameter("pid/estimate_derivative", def_estimate_derivative) );
        params[3] = this->add( rclcpp::Parameter("pid/use_integral",        def_use_integral) );
        params[4] = this->add( rclcpp::Parameter("pid/use_prefilter",       def_use_prefilter) );
    }

    inline auto overshoot() const
    {
        return this->getParameterPool()[params[0]].as_double();
    }

    inline auto settling() const
    {
        return this->getParameterPool()[params[1]].as_double();
    }

    inline auto estimate_derivative() const
    {
        return this->getParameterPool()[params[2]].as_bool();
    }

    inline auto use_integral() const
    {
        return this->getParameterPool()[params[3]].as_bool();
    }

    inline auto use_prefilter() const
    {
        return this->getParameterPool()[params[4]].as_bool();
    }
};

}
}
