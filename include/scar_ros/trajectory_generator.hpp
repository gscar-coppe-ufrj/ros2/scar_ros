/**
 * @brief Class created to create a node using only sine trajectory (for now) and 2 position derivatives (hardcoded)
 * @version 0.1
 * @date 2019-09-02
 *
 * @copyright Copyright (c) 2019
 *
 * TO DO
 * 1) Use other message types to consider SNAP and JERK while calculating the trajectory
 * 2) Create message using correct frame (disconsidered for now)
 * 3) Add coef_1*t + coef_2*t^2 parameters in trajectory
 */


#pragma once


#include <rclcpp/executors/single_threaded_executor.hpp>
#include <rclcpp_lifecycle/lifecycle_node.hpp>
#include <rclcpp/rclcpp.hpp>
#include <rclcpp/time.hpp>
#include <rclcpp/clock.hpp>
#include <geometry_msgs/msg/accel_stamped.hpp>
#include <nav_msgs/msg/odometry.hpp>
#include <rosgraph_msgs/msg/clock.hpp>
#include <scar/trajectory/TrajectorySine.hpp>
#include <scar/trajectory/TrajectoryTwoPoint.hpp>
#include <Eigen/Geometry>
#include <string>
#include <scar_ros/conversions/time.hpp>
#include <control_network/node.hpp>


namespace scar_ros
{


class TrajectoryGenerator : public rclcpp_lifecycle::LifecycleNode, public control_network::Node
{
private:

    std::shared_ptr<scar::trajectory::TrajectoryBase> traj;

    geometry_msgs::msg::AccelStamped msg_accel;
    nav_msgs::msg::Odometry msg_odom;
    rclcpp::QoS qos_trajectory;

    rclcpp_lifecycle::LifecyclePublisher<geometry_msgs::msg::AccelStamped>::SharedPtr pub_accel_cov;
    rclcpp_lifecycle::LifecyclePublisher<nav_msgs::msg::Odometry>::SharedPtr pub_odom;

    void resetPublishers();

    rclcpp::Parameter param_traj_type;
    rclcpp::Parameter param_amplitude;
    rclcpp::Parameter param_frequency;
    rclcpp::Parameter param_phase;
    rclcpp::Parameter param_offset;
    rclcpp::Parameter param_order;

    rclcpp::Parameter param_initial_data_dimensions;
    rclcpp::Parameter param_initial_time_sec;
    rclcpp::Parameter param_initial_data;
    rclcpp::Parameter param_final_data_dimensions;
    rclcpp::Parameter param_final_time_sec;
    rclcpp::Parameter param_final_data;
    rclcpp::Parameter param_max_vel;

public:
    explicit TrajectoryGenerator(bool intra_process_comms = false);
    ~TrajectoryGenerator();

    bool update(const rclcpp::Time& time) override;

public:
    CallbackReturn on_configure(const rclcpp_lifecycle::State&) override;
    CallbackReturn on_cleanup(const rclcpp_lifecycle::State&) override;
    CallbackReturn on_shutdown(const rclcpp_lifecycle::State&) override;
    CallbackReturn on_activate(const rclcpp_lifecycle::State&) override;
    CallbackReturn on_deactivate(const rclcpp_lifecycle::State&) override;


};
}
