#include "scar_ros/predictor.hpp"
#include "scar_ros/conversions/time.hpp"

#include <scar/predictor/PhaseLock.hpp>

#include <lifecycle_msgs/msg/state.hpp>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2/LinearMath/Matrix3x3.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>


using namespace ::scar_ros;
using rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface;


enum class Predictor::State : unsigned char
{
    UNCONFIGURED,
    INACTIVE,
    WAIT_FIRST,
    GATHER_WAYPOINTS
};

Predictor::Predictor(bool intra_process_comms) :
    LifecycleNode("predictor",
        rclcpp::NodeOptions().use_intra_process_comms(intra_process_comms)),
    qos_ctrl(rclcpp::QoS(rclcpp::KeepLast(5)).durability_volatile().best_effort()),
    param_frequencies_hz("frequencies_hz", std::vector<double>()),
    param_window_memory("window_memory", 1.0),
    param_window_prediction("window_prediction", 1.0),
    param_pose_sampling("pose_sampling", 0.01),
    param_target_frame("target_frame", ""),
    tf_buffer(get_clock()),
    state(State::UNCONFIGURED)
{
    {
        auto declare = [this] (const rclcpp::Parameter& param) {
            this->declare_parameter(param.get_name(), param.get_parameter_value());
        };
        declare(param_frequencies_hz);
        declare(param_window_memory);
        declare(param_window_prediction);
        declare(param_pose_sampling);
        declare(param_target_frame);
    }
}

void Predictor::onDataReceived(
    const nav_msgs::msg::Odometry::ConstSharedPtr& msg_odom,
    const geometry_msgs::msg::AccelWithCovarianceStamped::ConstSharedPtr& msg_accel
)
{
    scar::types::Time time;
    if (msg_odom)
        time = scar_ros::conversions::convert(msg_odom->header.stamp);

    switch (state)
    {
    case State::GATHER_WAYPOINTS:
        if (time <= time_last
            || waypoints_iter == waypoints.end()
            || (time - time_last).toSeconds() < param_pose_sampling.as_double())
            return;
        break;
    case State::WAIT_FIRST:
        if (msg_odom)
            state = State::GATHER_WAYPOINTS;
        else
            return;
        break;
    case State::UNCONFIGURED:
    case State::INACTIVE:
        return;
    }

    // Odometry
    frame_odom = msg_odom->header.frame_id;
    tf2::Quaternion quat = tf2::Quaternion(
        msg_odom->pose.pose.orientation.x,
        msg_odom->pose.pose.orientation.y,
        msg_odom->pose.pose.orientation.z,
        msg_odom->pose.pose.orientation.w
    );
    double roll, pitch, yaw;
    tf2::Matrix3x3(quat).getEulerYPR(yaw, pitch, roll);
    Eigen::Vector6d pose, twist;
    pose << msg_odom->pose.pose.position.x, msg_odom->pose.pose.position.y, msg_odom->pose.pose.position.z,
            roll, pitch, yaw;
    //
    twist << msg_odom->twist.twist.linear.x, msg_odom->twist.twist.linear.y, msg_odom->twist.twist.linear.z,
             msg_odom->twist.twist.angular.x, msg_odom->twist.twist.angular.y, msg_odom->twist.twist.angular.z;

    // Acceleration
    Eigen::Vector6d accel;
    if (msg_accel)
    {
        Eigen::Vector6d accel;
        accel << msg_accel->accel.accel.linear.x, msg_accel->accel.accel.linear.y, msg_accel->accel.accel.linear.z,
                 msg_accel->accel.accel.angular.x, msg_accel->accel.accel.angular.y, msg_accel->accel.accel.angular.z;
        frame_relation.update(pose, twist, accel);
        accel = frame_relation.ddot_pose;
    }
    else
    {
        frame_relation.update(pose, twist);
        accel.setZero();
    }

    scar::types::Waypoint::Data data(6, 3);
    data.col(0) = frame_relation.pose;
    data.col(1) = frame_relation.dot_pose;
    data.col(2) = frame_relation.ddot_pose;
    *(waypoints_iter++) = scar::types::Waypoint(data, time);
    time_last = time;
}

void Predictor::resetPublishers()
{
    pub_odom.reset();
    pub_quality_pos.reset();
    pub_quality_rot.reset();
}

LifecycleNodeInterface::CallbackReturn
Predictor::on_configure(const rclcpp_lifecycle::State&)
{
    // Only publishers are initialized here while there is no LifecycleSubscription
    // class implemented in ROS2. Otherwise, messages are received by the subscription
    // even though this node is not active
    pub_odom        = create_publisher<nav_msgs::msg::Odometry>("out", qos_ctrl);
    pub_quality_pos = create_publisher<std_msgs::msg::Float64>("quality/position", qos_ctrl);
    pub_quality_rot = create_publisher<std_msgs::msg::Float64>("quality/orientation", qos_ctrl);

    {
        auto get = [this] (rclcpp::Parameter& param) {
            this->get_parameter(param.get_name(), param);
        };
        get(param_frequencies_hz);
        get(param_window_memory);
        get(param_window_prediction);
        get(param_pose_sampling);
        get(param_target_frame);
    }

    auto data_frequencies = param_frequencies_hz.as_double_array();
    if (data_frequencies.size() != 6)
    {
        RCLCPP_ERROR(
            get_logger(),
            "the %s parameter must be a double vector with exactly 6 elements",
            param_frequencies_hz.get_name().c_str()
        );
        return CallbackReturn::FAILURE;
    }

    // Configure predictor
    scar::predictor::PhaseLock::vec_t frequencies_vec(data_frequencies.size());
    std::copy(data_frequencies.begin(), data_frequencies.end(), frequencies_vec.data());
    predictor_ptr = std::make_unique<scar::predictor::PhaseLock>(
        2 * M_PI * frequencies_vec,
        scar::types::Time::fromSeconds(param_window_memory.as_double()),
        scar::types::Time::fromSeconds(param_window_prediction.as_double())
    );
    unsigned int n_samples = static_cast<unsigned int>(
        param_window_prediction.as_double() / param_pose_sampling.as_double()
    );
    waypoints.resize(n_samples); // add 1 just to make sure there is no overflow

    // Configure FrameRelation
    frame_relation.setMode(scar::rbd::FrameRelation::Source, scar::rbd::FrameRelation::Dynamic);

    // Update state
    state = State::INACTIVE;

    return CallbackReturn::SUCCESS;
}

LifecycleNodeInterface::CallbackReturn
Predictor::on_cleanup(const rclcpp_lifecycle::State&)
{
    resetPublishers();

    // Update state
    state = State::UNCONFIGURED;

    return CallbackReturn::SUCCESS;
}

LifecycleNodeInterface::CallbackReturn
Predictor::on_shutdown(const rclcpp_lifecycle::State&)
{
    resetPublishers();
    return CallbackReturn::SUCCESS;
}

LifecycleNodeInterface::CallbackReturn
Predictor::on_activate(const rclcpp_lifecycle::State&)
{
    using namespace std::placeholders;

    // Activate LifecyclePublisher objects
    pub_odom->on_activate();
    pub_quality_pos->on_activate();
    pub_quality_rot->on_activate();

    // Subscribe and register callbacks
    sub_odom.subscribe(this, "odom", qos_ctrl.get_rmw_qos_profile());
    sub_accel.subscribe(this, "accel", qos_ctrl.get_rmw_qos_profile());
    sync = std::make_shared<SyncPolicy::Sync>(SyncPolicy::Sync(), sub_odom, sub_accel);
    sync->registerCallback(std::bind(&Predictor::onDataReceived, this, _1, _2));
    //
    tf_listener = std::make_shared<tf2_ros::TransformListener>(tf_buffer, shared_from_this(), false);
    frame_odom = "";

    // Reset waypoints iterator
    waypoints_iter = waypoints.begin();
    time_last = scar::types::Time();

    // Update state
    state = State::WAIT_FIRST;

    return CallbackReturn::SUCCESS;
}

LifecycleNodeInterface::CallbackReturn
Predictor::on_deactivate(const rclcpp_lifecycle::State&)
{
    // Release sync policy
    sync.reset();

    // Deactivate LifecyclePublisher objects
    pub_odom->on_deactivate();
    pub_quality_pos->on_deactivate();
    pub_quality_rot->on_deactivate();

    // Unsubscribe
    sub_odom.unsubscribe();
    sub_accel.unsubscribe();

    // Clear transform stuff
    tf_listener.reset();
    tf_buffer.clear();

    // Clear waypoints
    waypoints.clear();

    // Update state
    state = State::INACTIVE;

    return CallbackReturn::SUCCESS;
}

bool Predictor::update(const rclcpp::Time& time_ros)
{
    if (state != State::UNCONFIGURED && state != State::INACTIVE)
        sync->getPolicy()->call();

    switch (state)
    {
    case State::GATHER_WAYPOINTS:
        break;
    case State::UNCONFIGURED:
    case State::INACTIVE:
    case State::WAIT_FIRST:
        return false;
    }

    auto time = scar_ros::conversions::convert(time_ros);
    auto time_prediction = time + scar::types::Time::fromSeconds(0);
    if (predictor_ptr->isValidTime(time_prediction))
    {
        if (waypoints_quality_iter != waypoints_iter)
        {
            predictor_ptr->updateQuality(waypoints_quality_iter, waypoints_iter);
            waypoints_quality_iter = waypoints_iter;
            std_msgs::msg::Float64 msg_quality_pos, msg_quality_rot;
            msg_quality_pos.data = static_cast<Eigen::Vector3d>(
                predictor_ptr->getQuality().block<3, 1>(0, 0)
            ).squaredNorm();
            msg_quality_rot.data = static_cast<Eigen::Vector3d>(
                predictor_ptr->getQuality().block<3, 1>(3, 0)
            ).squaredNorm();
            pub_quality_pos->publish(msg_quality_pos);
            pub_quality_rot->publish(msg_quality_rot);
        }

        auto wp = predictor_ptr->get(time_prediction);

        // Rotation
        tf2::Quaternion rot;
        rot =   tf2::Quaternion(tf2::Vector3(0, 0, 1), wp.getData()(5, 0))
              * tf2::Quaternion(tf2::Vector3(0, 1, 0), wp.getData()(4, 0))
              * tf2::Quaternion(tf2::Vector3(1, 0, 0), wp.getData()(3, 0));

        // Position
        tf2::Vector3 pos(wp.getData()(0,0), wp.getData()(1,0), wp.getData()(2,0));

        // Transform to target
        tf2::Transform odom_tf(rot, pos);
        tf2::TimePoint tf2_time(std::chrono::nanoseconds(0)); // get the latest
        tf2::Transform target_tf;
        try
        {
            tf2::fromMsg(
                tf_buffer.lookupTransform(frame_odom, param_target_frame.as_string(), tf2_time).transform,
                target_tf
            );
        }
        catch (tf2::LookupException e)
        {
            RCLCPP_WARN(get_logger(), e.what());
            target_tf.setIdentity();
        }
        odom_tf = odom_tf * target_tf;

        nav_msgs::msg::Odometry msg_odom;
        msg_odom.header.frame_id = frame_odom;
        msg_odom.child_frame_id = param_target_frame.as_string();
        msg_odom.header.stamp = scar_ros::conversions::convert(time_prediction);
        //
        msg_odom.pose.pose.position.x = odom_tf.getOrigin().getX();
        msg_odom.pose.pose.position.y = odom_tf.getOrigin().getY();
        msg_odom.pose.pose.position.z = odom_tf.getOrigin().getZ();
        msg_odom.pose.pose.orientation.x = odom_tf.getRotation().getX();
        msg_odom.pose.pose.orientation.y = odom_tf.getRotation().getY();
        msg_odom.pose.pose.orientation.z = odom_tf.getRotation().getZ();
        msg_odom.pose.pose.orientation.w = odom_tf.getRotation().getW();

        pub_odom->publish(msg_odom);
        return true;
    }

    auto time_diff = time_last - waypoints.begin()->getTime();
    if (time_diff.toSeconds() < param_window_memory.as_double())
        return true;

    predictor_ptr->update(waypoints.begin(), waypoints_iter, time_last);
    waypoints_iter = waypoints.begin();
    waypoints_quality_iter = waypoints_iter;

    return true;
}
