#include "scar_ros/simple_pose_control.hpp"
#include "scar_ros/conversions/time.hpp"

#include <rclcpp/executors/single_threaded_executor.hpp>
#include <lifecycle_msgs/msg/state.hpp>

#include <scar/types/Time.hpp>


using namespace ::scar_ros;


int main(int argc, char** argv)
{
    using scar::types::Time;

    rclcpp::init(argc, argv);
    rclcpp::executors::SingleThreadedExecutor exe;
    std::shared_ptr<rclcpp_lifecycle::LifecycleNode> node = std::make_shared<SimplePoseControl>();

    rclcpp::Duration duration(0, 10000000);
    rclcpp::Time last = node->now();

    while (rclcpp::ok())
    {
        exe.spin_node_some(node->get_node_base_interface());
        if (node->get_current_state().id() == lifecycle_msgs::msg::State::PRIMARY_STATE_ACTIVE)
        {
            auto control_network_node = std::dynamic_pointer_cast<control_network::Node>(node);
            if (control_network_node)
                control_network_node->update(last);
        }

        rclcpp::Duration elapsed = node->now() - last;
        if (duration > elapsed)
            rclcpp::sleep_for((duration - elapsed).to_chrono<std::chrono::nanoseconds>());
        last = node->now();
    }

    rclcpp::shutdown();
    return 0;
}
