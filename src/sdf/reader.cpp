#include "scar_ros/sdf/reader.hpp"

#include <experimental/filesystem>

#include <rclcpp/logging.hpp>


namespace scar_ros
{
namespace sdf
{

::sdf::SDFPtr read(
    const rclcpp::Parameter& param_sdf_file,
    const rclcpp::Parameter& param_sdf_string,
    rclcpp::Logger logger)
{
    bool has_sdf_file   = !param_sdf_file.as_string().empty();
    bool has_sdf_string = !param_sdf_string.as_string().empty();
    if (!has_sdf_file && !has_sdf_string)
    {
        RCLCPP_WARN(
            logger,
            "must provide either an SDF file path using the %s parameter"
                " or an SDF string using the %s parameter",
            param_sdf_file.get_name().c_str(),
            param_sdf_string.get_name().c_str()
        );
        return nullptr;
    }
    //
    if (has_sdf_file && has_sdf_string)
        RCLCPP_WARN(
            logger,
            "received both an SDF filename and an SDF string; defaluting to read from the file"
        );
    //
    ::sdf::SDFPtr sdf = nullptr;
    if (has_sdf_file)
    {
        // get model path
        std::string model = expandPath(param_sdf_file.as_string());

        if (model.empty())
        {
            RCLCPP_WARN(
                logger,
                "unable to load SDF file: model %s not found in the Gazebo search path",
                param_sdf_file.as_string().c_str()
            );
            return nullptr;
        }

        // open SDF file
        sdf = ::sdf::readFile(model);

        if (!sdf)
        {
            RCLCPP_WARN(
                logger,
                "unable to load SDF file: failed to open SDF file %s",
                param_sdf_file.as_string().c_str()
            );
            return nullptr;
        }
    }
    else if (has_sdf_string)
    {
        sdf = std::make_shared<::sdf::SDF>();
        ::sdf::init(sdf);
        if (!::sdf::readString(param_sdf_string.as_string(), sdf))
        {
            RCLCPP_WARN(
                logger,
                "unable to load SDF file: failed to read SDF string given in parameter %s",
                param_sdf_string.get_name().c_str()
            );
            return nullptr;
        }
    }
    else
    {
        RCLCPP_WARN(logger, "unable to load SDF file: no SDF file or SDF string provided");
        return nullptr;
    }
    return sdf;
}

std::string expandPath(const std::string& model_path)
{
    // check if model_path does not start with model://
    std::string prefix = "model://";
    if (model_path.find(prefix) == std::string::npos)
        return model_path;

    using fs_path = std::experimental::filesystem::path;
    fs_path model_file(model_path.substr(prefix.size()));
    model_file = model_file / fs_path("model.sdf");

    // parse GAZEBO_MODEL_PATH environment variable
    char const* val = getenv("GAZEBO_MODEL_PATH");
    std::string gazebo_model_path = (val == nullptr) ? "." : ".:" + std::string(val);

    std::istringstream f(gazebo_model_path);
    std::string path, ret;
    while (std::getline(f, path, ':'))
    {
        fs_path dir(path);
        fs_path full_path = dir / model_file;
        if (std::experimental::filesystem::exists(full_path))
            return full_path.string();
    }
    return "";
}


}
}
