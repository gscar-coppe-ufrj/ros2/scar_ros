#include "scar_ros/sdf/link.hpp"

#include <rclcpp/logging.hpp>


using namespace ::scar_ros::sdf;



Link::Link(const ::sdf::SDFPtr& sdf, const std::string& link_name, rclcpp::Logger logger) :
    ok_(true), mass_(0)
{
    ::sdf::ElementPtr model = sdf->Root()->GetElement("model");
    //
    bool found = false;
    ::sdf::ElementPtr link = model->GetElement("link");
    while (link)
    {
        found = link->Get<std::string>("name") == link_name;
        if (found)
            break;
        link = link->GetNextElement("link");
    }
    if (!found)
    {
        RCLCPP_WARN(
            logger,
            "unable to find link %s for model %s",
            link_name.c_str(),
            model->Get<std::string>("name").c_str()
        );
        ok_ = false;
        return;
    }
    if (!link->HasElement("inertial"))
    {
        inertia_.setIdentity();
        mass_ = 1;
    }
    else
    {
        ::sdf::ElementPtr inertial = link->GetElement("inertial");
        if (!inertial->HasElement("inertia"))
            inertia_.setIdentity();
        else
        {
            auto inertia = inertial->GetElement("inertia");
            //
            inertia_(0,0) = inertia->Get<double>("ixx", 1.0).first;
            inertia_(0,1) = inertia->Get<double>("ixy", 0.0).first;
            inertia_(0,2) = inertia->Get<double>("ixz", 0.0).first;
            //
            inertia_(1,0) = inertia_(0,1);
            inertia_(1,1) = inertia->Get<double>("iyy", 1.0).first;
            inertia_(1,2) = inertia->Get<double>("iyz", 0.0).first;
            //
            inertia_(2,0) = inertia_(0,2);
            inertia_(2,1) = inertia_(1,2);
            inertia_(2,2) = inertia->Get<double>("izz", 1.0).first;
        }
        mass_ = inertial->Get<double>("mass", 1.0).first;
    }
}
