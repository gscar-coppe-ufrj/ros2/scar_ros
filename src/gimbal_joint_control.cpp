﻿#include "scar_ros/gimbal_joint_control.hpp"
#include "scar_ros/conversions/time.hpp"
#include "scar_ros/sdf/link.hpp"
#include "scar_ros/sdf/reader.hpp"
#include "scar_ros/control/pid_parameters.hpp"

#include <lifecycle_msgs/msg/state.hpp>
#include <tf2_eigen/tf2_eigen.h>

#include <sdf/sdf.hh>
#include <scar/controller/SettingsPIDSecondOrder.hpp>
#include <scar/rbd/FrameRelation.hpp>


using namespace ::scar_ros;
using rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface;


GimbalJointControl::GimbalJointControl(bool intra_process_comms) :
    LifecycleNode(
        "scar_gimbal_joint_control",
        rclcpp::NodeOptions().use_intra_process_comms(intra_process_comms)
    ),
    pid(nullptr),
    rigid_bodies(),
    ref_joint_pos(), ref_joint_vel(), ref_ff(), joint_pos(), joint_vel(),
    qos_ctrl(rclcpp::QoS(rclcpp::KeepLast(5)).durability_volatile().best_effort())
{
    ref_joint_pos.setZero();
    ref_joint_vel.setZero();
    ref_ff.setZero();
    joint_pos.setZero();
    joint_vel.setZero();

    // Parameters
    param_sdf_file   = rclcpp::Parameter("sdf/file", "");
    param_sdf_string = rclcpp::Parameter("sdf/string", "");
    param_sdf_link   = rclcpp::Parameter("sdf/link", std::vector<std::string>({""}));
    param_sdf_joint  = rclcpp::Parameter("sdf/joint", std::vector<std::string>({""}));
    //
    declare_parameter<std::string>(param_sdf_file);
    declare_parameter<std::string>(param_sdf_string);
    declare_parameter<std::vector<std::string>>(param_sdf_link);
    declare_parameter<std::vector<std::string>>(param_sdf_joint);
    //
    params_pid = std::make_shared<control::PIDParameters<rclcpp_lifecycle::LifecycleNode>>(this);
    params_pid->declare();
}

void GimbalJointControl::onDataReceived(
    const sensor_msgs::msg::JointState::ConstSharedPtr& msg_joint_state,
    const sensor_msgs::msg::JointState::ConstSharedPtr& msg_joint_state_ref)
{
    auto msg_size = 0;
    if (!msg_joint_state || !msg_joint_state_ref)
    {
        has_null = true;
        return;
    }
    else if (msg_joint_state->name.size() != msg_joint_state_ref->name.size())
    {
        has_null = false;
        RCLCPP_ERROR(this->get_logger(), "Joint state msgs differ in size");
        return;
    }
    else
    {
        msg_size = msg_joint_state_ref->name.size();
        has_null = false;
    }

    ref_joint_pos.resize(msg_size, 1);
    ref_joint_vel.resize(msg_size, 1);
    joint_pos.resize(msg_size, 1);
    joint_vel.resize(msg_size, 1);
    accel.resize(msg_size, 1);

    std::copy(msg_joint_state_ref->position.begin(), msg_joint_state_ref->position.end(), ref_joint_pos.data());
    std::copy(msg_joint_state_ref->velocity.begin(), msg_joint_state_ref->velocity.end(), ref_joint_vel.data());
    std::copy(msg_joint_state->position.begin(), msg_joint_state->position.end(), joint_pos.data());
    std::copy(msg_joint_state->velocity.begin(), msg_joint_state->velocity.end(), joint_vel.data());
}

void GimbalJointControl::resetPublishers()
{
    pub_cmd.reset();
    pub_error.reset();
}

LifecycleNodeInterface::CallbackReturn
GimbalJointControl::on_configure(const rclcpp_lifecycle::State&)
{
    // Only publishers are initialized here while there is no LifecycleSubscription
    // class implemented in ROS2. Otherwise, messages are received by the subscription
    // even though this node is not active
    pub_cmd   = create_publisher<sensor_msgs::msg::JointState>("cmd", qos_ctrl);
    pub_error = create_publisher<sensor_msgs::msg::JointState>("error", qos_ctrl);

    // Retrieve parameters
    try
    {
        get_parameter(param_sdf_file);
    }
    catch(const std::exception& e)
    {
        std::cout << "Got error in file" << std::endl;
        std::cerr << e.what() << '\n';
    }

    get_parameter(param_sdf_string);

    try
    {
        get_parameter(param_sdf_link);
    }
    catch(const std::exception& e)
    {
        std::cout << "Gor error in link" << std::endl;
        std::cerr << e.what() << '\n';
    }

    try
    {
        get_parameter(param_sdf_joint);
    }
    catch(const std::exception& e)
    {
        std::cout << "Gor error in joint" << std::endl;
        std::cerr << e.what() << '\n';
    }

    pid = std::make_unique<scar::controller::PID>(param_sdf_joint.as_string_array().size());
    rigid_bodies.resize(param_sdf_link.as_string_array().size());
    // joint_names.resize(param_sdf_joint.as_string_array().size());
    joint_names = param_sdf_joint.as_string_array();
    //
    params_pid->read();

    // Read SDF
    if (param_sdf_link.as_string_array().empty())
    {
        RCLCPP_WARN(get_logger(), "must provide a link to search for using the %s parameter", param_sdf_link.get_name().c_str());
        return CallbackReturn::FAILURE;
    }

     ::sdf::SDFPtr sdf = scar_ros::sdf::read(param_sdf_file, param_sdf_string, get_logger());
    if (!sdf)
        return CallbackReturn::FAILURE;
    // Read links from sdf
    for (size_t i = 0; i < param_sdf_link.as_string_array().size(); ++i) {
        scar_ros::sdf::Link sdf_link(sdf, param_sdf_link.as_string_array()[i], get_logger());
        if (!sdf_link.ok())
            return CallbackReturn::FAILURE;
        rigid_bodies[i].mass = sdf_link.mass();
        rigid_bodies[i].inertia_matrix = sdf_link.inertia();
    }

    // Configure PID
    using namespace scar::controller;
    pid->restart();
    SettingsPID pid_settings;
    pid_settings = SettingsPIDSecondOrder::createFromSpecT(
        params_pid->overshoot(),
        params_pid->settling()
    );
    pid->configure(pid_settings, scar::types::Time::fromSeconds(params_pid->sampling()));
    pid->setDerivativeFiltered( params_pid->estimate_derivative());
    pid->setCallbackPostProcessing(
        [this] (scar::controller::Output& out) -> void {
            double inertia_z;
            double inertia_y;
            double inertia_x;
            inertia_z = this->rigid_bodies[0].inertia_matrix(2,2) + 
                        this->rigid_bodies[1].inertia_matrix(2,2) +
                        this->rigid_bodies[2].inertia_matrix(2,2) + 
                        this->rigid_bodies[3].inertia_matrix(2,2);
            inertia_y = this->rigid_bodies[1].inertia_matrix(1,1) + 
                        this->rigid_bodies[2].inertia_matrix(1,1) +
                        this->rigid_bodies[3].inertia_matrix(1,1);
            inertia_x = this->rigid_bodies[2].inertia_matrix(0,0) +
                        this->rigid_bodies[3].inertia_matrix(0,0);
            out[0] = out[0] * inertia_z;
            out[1] = out[1] * inertia_y;
            out[2] = out[2] * inertia_x;
        }
    );
    return CallbackReturn::SUCCESS;
}

LifecycleNodeInterface::CallbackReturn
GimbalJointControl::on_cleanup(const rclcpp_lifecycle::State&)
{
    resetPublishers();
    return CallbackReturn::SUCCESS;
}

LifecycleNodeInterface::CallbackReturn
GimbalJointControl::on_shutdown(const rclcpp_lifecycle::State&)
{
    resetPublishers();
    return CallbackReturn::SUCCESS;
}

LifecycleNodeInterface::CallbackReturn
GimbalJointControl::on_activate(const rclcpp_lifecycle::State&)
{
    using namespace std::placeholders;

    // Activate LifecyclePublisher objects
    pub_cmd->on_activate();
    pub_error->on_activate();

    sub_joint_state.subscribe(this, "joint_state", qos_ctrl.get_rmw_qos_profile());
    sub_joint_state_ref.subscribe(this, "joint_state_ref", qos_ctrl.get_rmw_qos_profile());
    sync = std::make_shared<SyncPolicy::Sync>(SyncPolicy::Sync(), sub_joint_state, sub_joint_state_ref);
    sync->registerCallback(std::bind(&GimbalJointControl::onDataReceived, this, _1, _2));

    has_null = false;

    return CallbackReturn::SUCCESS;
}

LifecycleNodeInterface::CallbackReturn
GimbalJointControl::on_deactivate(const rclcpp_lifecycle::State&)
{
    // Release sync policy
    sync = nullptr;

    // Send zero force/torque
    sensor_msgs::msg::JointState msg_out;
    pub_cmd->publish(msg_out);

    // Deactivate LifecyclePublisher objects
    pub_cmd->on_deactivate();
    pub_error->on_deactivate();

    // Unsubscribe
    sub_joint_state.unsubscribe();
    sub_joint_state_ref.unsubscribe();

    // Restart the PID
    pid->restart();

    return CallbackReturn::SUCCESS;
}

bool GimbalJointControl::update(const rclcpp::Time& time)
{
    if (get_current_state().id() != lifecycle_msgs::msg::State::PRIMARY_STATE_ACTIVE)
        return false;
    sync->getPolicy()->call();
    if (has_null)
        return false;

    if (!pid->isDerivativeFiltered())
        pid->setErrorDerivative(ref_joint_vel - joint_vel);

    pid->update(scar_ros::conversions::convert(time), ref_joint_pos, joint_pos);

    auto out = pid->getOutput();

    auto N = joint_pos.size();
    // TODO(jcmonteiro) test if this msg is correctly configured
    sensor_msgs::msg::JointState msg_out;
    sensor_msgs::msg::JointState msg_error;

    msg_error.name.resize(N);
    msg_error.position.resize(N);
    msg_error.velocity.resize(N);
    msg_error.effort.resize(N);

    msg_out.name.resize(N);
    msg_out.position.resize(N);
    msg_out.velocity.resize(N);
    msg_out.effort.resize(N);

    scar::controller::Input error(N), error_vel(N);

    error = ref_joint_pos- joint_pos;
    error_vel = pid->getErrorDerivative();

    std::copy(joint_names.begin(), joint_names.end(), msg_out.name.begin());
    std::copy(out.data(), out.data() + N, msg_out.effort.begin());
    std::copy(error.data(), error.data() + N, msg_error.position.begin());
    std::copy(error_vel.data(), error_vel.data() + N, msg_error.velocity.begin());

    msg_error.header.stamp = time;
    msg_out.header.stamp = time;

    pub_cmd->publish(msg_out);
    pub_error->publish(msg_error);

    return true;
}
