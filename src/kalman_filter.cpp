﻿#include "scar_ros/kalman_filter.hpp"
#include "scar_ros/conversions/time.hpp"
#include "scar_ros/conversions/gps_common.hpp"

#include <Eigen/Eigen>
#include <sdf/sdf.hh>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>

#include <scar/linear-system/Builder.hpp>


using namespace ::scar_ros;


KalmanFilter::KalmanFilter(bool intra_process_comms) :
    LifecycleNode("kalman_filter",
        rclcpp::NodeOptions().use_intra_process_comms(intra_process_comms)),
    qos_ctrl(rclcpp::QoS(rclcpp::KeepLast(5)).durability_volatile().best_effort()),
    param_frame_world("frame_world", ""),
    param_frame_odom("frame_odom", ""),
    param_frame_body("frame_body", ""),
    param_frame_gps("frame_gps", ""),
    param_frame_imu("frame_imu", ""),
    param_filter_cutoff("filter/cutoff", std::nan("")),
    param_filter_sampling("filter/sampling", std::nan("")),
    param_filter_use("filter/use", false),
    tf_buffer(get_clock())
{
    auto declare_parameters = [this] (std::initializer_list<rclcpp::Parameter> params) -> void {
        for (auto param : params)
            this->declare_parameter(param.get_name(), param.get_parameter_value());
    };
    declare_parameters({
        param_frame_world,
        param_frame_odom,
        param_frame_body,
        param_frame_gps,
        param_frame_imu,
        param_filter_cutoff,
        param_filter_sampling,
        param_filter_use
    });
}

void KalmanFilter::onDataReceived(
    const sensor_msgs::msg::NavSatFix::ConstSharedPtr& msg_gps,
    const sensor_msgs::msg::Imu::ConstSharedPtr& msg_imu
)
{
    if (!msg_gps || !msg_imu)
    {
        has_null = true;
        return;
    }
    else if (has_null)
        has_null = false;

    // Passing messages
    this->msg_gps = *msg_gps;
    this->msg_imu = *msg_imu;
}

void KalmanFilter::resetPublishers()
{
    pub_odom.reset();
    pub_accel.reset();
}

rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn
KalmanFilter::on_configure(const rclcpp_lifecycle::State&)
{
    using nav_msgs::msg::Odometry;
    using geometry_msgs::msg::AccelWithCovarianceStamped;

    // Only publishers are initialized here while there is no LifecycleSubscription
    // class implemented in ROS2. Otherwise, messages are received by the subscription
    // even though this node is not active
    pub_odom  = create_publisher<Odometry>("odom", qos_ctrl);
    pub_accel = create_publisher<AccelWithCovarianceStamped>("accel", qos_ctrl);

    // Retrieve parameters
    auto get_parameter = [this] (rclcpp::Parameter& param) {
        this->get_parameter(param.get_name(), param);
    };
    //
    get_parameter(param_frame_world);
    get_parameter(param_frame_odom);
    get_parameter(param_frame_body);
    get_parameter(param_frame_gps);
    get_parameter(param_frame_imu);
    get_parameter(param_filter_cutoff);
    get_parameter(param_filter_sampling);
    get_parameter(param_filter_use);

    // Fill odometry and accel headers
    msg_cmd_header.frame_id = param_frame_odom.as_string();

    // Configure velocity filter
    if (param_filter_use.as_bool())
    {
        double wc = param_filter_cutoff.as_double();
        double ts = param_filter_sampling.as_double();

        if (std::isnan(wc) || std::isnan(ts))
        {
            RCLCPP_WARN(
                get_logger(),
                "both cutoff frequency (parameter %s) and sampling period (parameter %s) must "
                    "be set to positive values to use the filter",
                param_filter_cutoff.get_name().c_str(),
                param_filter_sampling.get_name().c_str()
            );
            return rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn::FAILURE;
        }

        if (wc <= 0)
        {
            RCLCPP_WARN(
                get_logger(),
                "cutoff frequency (parameter %s) must be positive",
                param_filter_cutoff.get_name().c_str()
            );
            return rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn::FAILURE;
        }

        using scar::linear_system::LinearSystem;
        using scar::types::Poly;
        Poly num(2), den(2);
        num << 1, 0;
        den << 1 / wc, 1;
        filter_vel = std::make_shared<LinearSystem>(num, den, scar::types::Time::fromSeconds(ts));
        filter_vel->useNFilters(6);
    }

    return rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn::SUCCESS;
}

rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn
KalmanFilter::on_cleanup(const rclcpp_lifecycle::State&)
{
    resetPublishers();
    return rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn::SUCCESS;
}

rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn
KalmanFilter::on_shutdown(const rclcpp_lifecycle::State&)
{
    resetPublishers();
    return rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn::SUCCESS;
}

rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn
KalmanFilter::on_activate(const rclcpp_lifecycle::State&)
{
    using namespace std::placeholders;
    using namespace nav_msgs::msg;
    using namespace geometry_msgs::msg;

    // Activate LifecyclePublisher objects
    pub_odom->on_activate();
    pub_accel->on_activate();

    // Subscribe and register callbacks
    sub_gps.subscribe(this, "gps", qos_ctrl.get_rmw_qos_profile());
    sub_imu.subscribe(this, "imu", qos_ctrl.get_rmw_qos_profile());
    sync = std::make_shared<SyncPolicy::Sync>(SyncPolicy::Sync(), sub_gps, sub_imu);
    sync->registerCallback(std::bind(&KalmanFilter::onDataReceived, this, _1, _2));
    //
    tf_listener = std::make_shared<tf2_ros::TransformListener>(tf_buffer, shared_from_this(), false);
    tf_broadcaster = std::make_shared<tf2_ros::TransformBroadcaster>(shared_from_this());

    has_null = true;
    first_run = true;

    return rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn::SUCCESS;
}

rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn
KalmanFilter::on_deactivate(const rclcpp_lifecycle::State&)
{
    // Release sync policy
    sync.reset();

    // Deactivate LifecyclePublisher objects
    pub_odom->on_deactivate();
    pub_accel->on_deactivate();

    // Unsubscribe
    sub_gps.unsubscribe();
    sub_imu.unsubscribe();

    // Clear transform stuff
    tf_listener.reset();
    tf_broadcaster.reset();
    tf_buffer.clear();

    return rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn::SUCCESS;
}

bool KalmanFilter::update(const rclcpp::Time& time)
{
    // Synchronize messages
    sync->getPolicy()->call();
    if (has_null)
        return false;

    // Return if there are no "odom-world", "body-imu", or "body-gps" transforms
    tf2::Transform  tf_odom_world, tf_body_imu, tf_body_gps;
    try
    {
        tf2::TimePoint time_tf2;
        conversions::convert(time, time_tf2);

        auto get_transform = [time_tf2, this] (const rclcpp::Parameter& target, const rclcpp::Parameter& source, tf2::Transform& tf) {
            tf2::fromMsg(
                tf_buffer.lookupTransform(
                    target.as_string(),
                    source.as_string(),
                    time_tf2
                ).transform,
                tf
            );
        };

        get_transform(param_frame_odom, param_frame_world, tf_odom_world);
        get_transform(param_frame_body, param_frame_imu,   tf_body_imu);
        get_transform(param_frame_body, param_frame_gps,   tf_body_gps);
    }
    catch (tf2::LookupException e)
    {
        RCLCPP_WARN(get_logger(), e.what());
        return false;
    }
    catch (tf2::InvalidArgumentException e)
    {
        RCLCPP_WARN(get_logger(), e.what());
        return false;
    }

    // Convert IMU orientation from a rotation from world to IMU to a rotation from odometry to body
    tf2::Quaternion att_world_imu;
    tf2::fromMsg(msg_imu.orientation, att_world_imu);
    tf2::Quaternion att_odom_body = tf_odom_world.getRotation() * att_world_imu * tf_body_imu.getRotation().inverse();
    //
    tf2::Vector3 angvel_body_in_imu_frame(msg_imu.angular_velocity.x, msg_imu.angular_velocity.y, msg_imu.angular_velocity.z);
    tf2::Vector3 angvel_body_in_body_frame = tf2::quatRotate(tf_body_imu.getRotation(), angvel_body_in_imu_frame);

    // Convert GPS data (p_{world,gps}) so that it corresponds to
    // data from the odometry to the body frame (p_{odom,body})
    // p_{odom,body} = p_{odom,world} + p_{world,gps} + p_{gps,body}
    double x, y;
    conversions::UTM(msg_gps, &x, &y);
    tf2::Vector3 pos_gps(x, y, msg_gps.altitude);
    tf2::Vector3 pos_odom_body = tf_odom_world * pos_gps - tf2::quatRotate(att_odom_body, tf_body_gps.getOrigin());

    // Estimate the linear velocity and angular acceleration
    tf2::Vector3 linear_velocity, angaccel_body_in_body_frame;
    if (param_filter_use.as_bool())
    {
        auto n_filters = filter_vel->getNFilters();
        auto filter_time = conversions::convert(time);
        scar::linear_system::Input filter_in(n_filters);
        filter_in << pos_odom_body.x(), pos_odom_body.y(), pos_odom_body.z(),
            angvel_body_in_body_frame.x(),
            angvel_body_in_body_frame.y(),
            angvel_body_in_body_frame.z();
        if (first_run)
        {
            Eigen::MatrixXd init_in(n_filters, 1), init_out_dout(n_filters, 1);
            init_in << pos_odom_body.x(), pos_odom_body.y(), pos_odom_body.z(),
                angvel_body_in_body_frame.x(),
                angvel_body_in_body_frame.y(),
                angvel_body_in_body_frame.z();
            init_out_dout.setZero();
            filter_vel->setInitialConditions(init_in, init_out_dout);
            filter_vel->setInitialTime(filter_time);
            first_run = false;
        }
        filter_vel->update(filter_in, filter_time);
        linear_velocity.setValue(
            filter_vel->getOutput()[0],
            filter_vel->getOutput()[1],
            filter_vel->getOutput()[2]
        );
        angaccel_body_in_body_frame.setValue(
          filter_vel->getOutput()[3],
          filter_vel->getOutput()[4],
          filter_vel->getOutput()[5]
        );
    }
    else
    {
        // TODO(jcmonteiro) do something else here; this is definetely not ideal
        linear_velocity.setValue(0, 0, 0);
        angaccel_body_in_body_frame.setValue(0, 0, 0);
    }

    // Compute linear acceleration in the body frame
    tf2::Vector3 accel_imu_in_imu_frame(
        msg_imu.linear_acceleration.x,
        msg_imu.linear_acceleration.y,
        msg_imu.linear_acceleration.z
    );
    auto accel_imu_in_body_frame = tf2::quatRotate(tf_body_imu.getRotation(), accel_imu_in_imu_frame);
    tf2::Vector3 accel_body_in_body_frame = accel_imu_in_body_frame -
        tf2::tf2Cross(angaccel_body_in_body_frame, tf_body_imu.getOrigin()) -
        tf2::tf2Cross(angvel_body_in_body_frame, tf2::tf2Cross(
            angvel_body_in_body_frame,
            tf_body_imu.getOrigin()
        ));

    // Helper function to fill messages
    auto write_tf2 = [] (const tf2::Vector3& vec, auto& pt) -> void {
        pt.x = vec.x();
        pt.y = vec.y();
        pt.z = vec.z();
    };

    // Fill pose
    write_tf2(pos_odom_body, msg_odom.pose.pose.position);
    msg_odom.pose.pose.orientation = tf2::toMsg(att_odom_body);
    // TODO(jcmonteiro) fix covariance based on the frame transformation
    Eigen::Matrix<double,6,6> odom_pose_cov;
    odom_pose_cov.block<3, 3>(0, 0) =
        Eigen::Map<Eigen::Matrix3d>(msg_gps.position_covariance.data());
    odom_pose_cov.block<3, 3>(0, 3) = odom_pose_cov.block<3, 3>(3, 0) = Eigen::Matrix3d::Zero();
    odom_pose_cov.block<3, 3>(3, 3) =
        Eigen::Map<Eigen::Matrix3d>(msg_imu.orientation_covariance.data());
    std::copy(odom_pose_cov.data(), odom_pose_cov.data() + odom_pose_cov.size(),
        msg_odom.pose.covariance.begin());

    // Fill velocities
    write_tf2(linear_velocity, msg_odom.twist.twist.linear);
    write_tf2(angvel_body_in_body_frame, msg_odom.twist.twist.angular);
    // TODO(jcmonteiro) fix covariance based on the frame transformation
    Eigen::Matrix<double,6,6> odom_vel_cov;
    odom_vel_cov.block<3, 3>(0, 0) = odom_vel_cov.block<3, 3>(0, 3) = odom_vel_cov.block<3, 3>(3, 0) =
        Eigen::Matrix3d::Zero();
    odom_vel_cov.block<3, 3>(3, 3) =
        Eigen::Map<Eigen::Matrix3d>(msg_imu.angular_velocity_covariance.data());
    std::copy(odom_vel_cov.data(), odom_vel_cov.data() + odom_vel_cov.size(),
        msg_odom.twist.covariance.begin());

    // Fill accelerations
    // The angular acceleration is not measured, therefore is not set
    write_tf2(angaccel_body_in_body_frame, msg_accel.accel.accel.angular);
    write_tf2(accel_body_in_body_frame, msg_accel.accel.accel.linear);
    // TODO(jcmonteiro) fix covariance based on the frame transformation
    Eigen::Matrix<double,6,6> odom_accel_cov;
    odom_accel_cov.block<3, 3>(0, 0) =
        Eigen::Map<Eigen::Matrix3d>(msg_imu.linear_acceleration_covariance.data());
    odom_accel_cov.block<3, 3>(0, 3) =
        odom_accel_cov.block<3, 3>(3, 0) = odom_accel_cov.block<3, 3>(3, 3) = Eigen::Matrix3d::Zero();
    std::copy(odom_accel_cov.data(), odom_accel_cov.data() + odom_accel_cov.size(),
        msg_accel.accel.covariance.begin());

    // Fill out headers
    msg_accel.header.stamp = time;
    msg_accel.header.frame_id = param_frame_body.as_string();
    msg_odom.header.stamp = time;
    msg_odom.header.frame_id = param_frame_odom.as_string();
    msg_odom.child_frame_id = param_frame_body.as_string();

    // Publish messages
    pub_odom->publish(msg_odom);
    pub_accel->publish(msg_accel);
    //
    geometry_msgs::msg::TransformStamped msg_tf_odom_body;
    msg_tf_odom_body.header.stamp = time;
    msg_tf_odom_body.header.frame_id = param_frame_odom.as_string();
    msg_tf_odom_body.child_frame_id = param_frame_body.as_string();
    msg_tf_odom_body.transform = tf2::toMsg( tf2::Transform(att_odom_body, pos_odom_body) );
    tf_broadcaster->sendTransform(msg_tf_odom_body);

    return true;
}
