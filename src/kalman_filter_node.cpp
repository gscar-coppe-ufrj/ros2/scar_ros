#include "scar_ros/kalman_filter.hpp"
#include "scar_ros/conversions/time.hpp"

#include <rclcpp/executors/single_threaded_executor.hpp>
#include <scar/types/Time.hpp>
#include "scar_ros/lifecycle_shutdown.hpp"


using namespace ::scar_ros;


int main(int argc, char** argv)
{
    using scar::types::Time;

    rclcpp::InitOptions init_options;
    init_options.shutdown_on_sigint = false;
    rclcpp::init(argc, argv, init_options);
    rclcpp::executors::SingleThreadedExecutor exe;

    std::shared_ptr<rclcpp_lifecycle::LifecycleNode> node = std::make_shared<KalmanFilter>();
    global_node = node;

    signal(SIGINT, shutdown_handler);

    rclcpp::Duration duration(0, 10000000);
    rclcpp::Time last = node->now();
    while (rclcpp::ok())
    {
        exe.spin_node_some(node->get_node_base_interface());
        if (node->get_current_state().id() == 3)
        {
            auto control_network_node = std::dynamic_pointer_cast<control_network::Node>(node);
            if (control_network_node)
                control_network_node->update(last);
        }

        rclcpp::Duration elapsed = node->now() - last;
        if (duration > elapsed)
            rclcpp::sleep_for((duration - elapsed).to_chrono<std::chrono::nanoseconds>());
        last = node->now();
    }

    rclcpp::shutdown();
    signal(SIGINT, SIG_DFL);
    global_node = nullptr;
    return 0;
}
