﻿#include "scar_ros/rigid_body_control.hpp"
#include "scar_ros/conversions/time.hpp"
#include "scar_ros/sdf/link.hpp"
#include "scar_ros/sdf/reader.hpp"
#include "scar_ros/control/pid_parameters.hpp"

#include <lifecycle_msgs/msg/state.hpp>
#include <tf2_eigen/tf2_eigen.h>

#include <sdf/sdf.hh>

#include <scar/controller/SettingsPIDFirstOrder.hpp>
#include <scar/controller/SettingsPIDSecondOrder.hpp>
#include <scar/rbd/FrameRelation.hpp>


using namespace ::scar_ros;
using rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface;


RigidBodyControl::RigidBodyControl(bool intra_process_comms) :
    LifecycleNode(
        "scar_rbd_control",
        rclcpp::NodeOptions().use_intra_process_comms(intra_process_comms)
    ),
    pid(6),
    body(),
    ref_pose(6), ref_twist(6), ref_ff(6), pose(6), twist(6), accel(6),
    qos_ctrl(rclcpp::QoS(rclcpp::KeepLast(5)).durability_volatile().best_effort())
{
    ref_pose.setZero();
    ref_twist.setZero();
    ref_ff.setZero();
    pose.setZero();
    twist.setZero();
    accel.setZero();

    // Parameters
    param_mode = rclcpp::Parameter("mode", "");
    //
    param_sdf_file   = rclcpp::Parameter("sdf/file", "");
    param_sdf_string = rclcpp::Parameter("sdf/string", "");
    param_sdf_link   = rclcpp::Parameter("sdf/link", "");
    //
    declare_parameter<std::string>(param_mode);
    declare_parameter<std::string>(param_sdf_file);
    declare_parameter<std::string>(param_sdf_string);
    declare_parameter<std::string>(param_sdf_link);
    //
    params_pid = std::make_shared<control::PIDParameters<rclcpp_lifecycle::LifecycleNode>>(this);
    params_pid->declare();
}

void RigidBodyControl::onDataReceived(
    const nav_msgs::msg::Odometry::ConstSharedPtr& msg_ref,
    const geometry_msgs::msg::AccelStamped::ConstSharedPtr& msg_ff,
    const nav_msgs::msg::Odometry::ConstSharedPtr& msg_odom,
    const geometry_msgs::msg::AccelWithCovarianceStamped::ConstSharedPtr& msg_accel
)
{
    if (!msg_ref || !msg_odom)
    {
        has_null = true;
        return;
    }
    else
        has_null = false;

    // Reference
    tf2::Quaternion quat(
        msg_ref->pose.pose.orientation.x,
        msg_ref->pose.pose.orientation.y,
        msg_ref->pose.pose.orientation.z,
        msg_ref->pose.pose.orientation.w
    );
    double roll, pitch, yaw;
    tf2::Matrix3x3(quat).getEulerYPR(yaw, pitch, roll);
    ref_pose << msg_ref->pose.pose.position.x, msg_ref->pose.pose.position.y, msg_ref->pose.pose.position.z,
            roll, pitch, yaw;
    //
    ref_twist << msg_ref->twist.twist.linear.x, msg_ref->twist.twist.linear.y, msg_ref->twist.twist.linear.z,
                 msg_ref->twist.twist.angular.x, msg_ref->twist.twist.angular.y, msg_ref->twist.twist.angular.z;

    // Feedforward
    if (msg_ff)
        ref_ff << msg_ff->accel.linear.x, msg_ff->accel.linear.y, msg_ff->accel.linear.z,
                  msg_ff->accel.angular.x, msg_ff->accel.angular.y, msg_ff->accel.angular.z;
    else
        ref_ff.setZero();

    // Odometry
    quat = tf2::Quaternion(
        msg_odom->pose.pose.orientation.x,
        msg_odom->pose.pose.orientation.y,
        msg_odom->pose.pose.orientation.z,
        msg_odom->pose.pose.orientation.w
    );
    tf2::Matrix3x3(quat).getEulerYPR(yaw, pitch, roll);
    pose << msg_odom->pose.pose.position.x, msg_odom->pose.pose.position.y, msg_odom->pose.pose.position.z,
            roll, pitch, yaw;
    //
    twist << msg_odom->twist.twist.linear.x, msg_odom->twist.twist.linear.y, msg_odom->twist.twist.linear.z,
             msg_odom->twist.twist.angular.x, msg_odom->twist.twist.angular.y, msg_odom->twist.twist.angular.z;

    // Acceleration
    if (msg_accel)
        accel << msg_accel->accel.accel.linear.x, msg_accel->accel.accel.linear.y, msg_accel->accel.accel.linear.z,
                 msg_accel->accel.accel.angular.x, msg_accel->accel.accel.angular.y, msg_accel->accel.accel.angular.z;
    else
        accel.setZero();
}

void RigidBodyControl::resetPublishers()
{
    pub_cmd.reset();
    pub_error.reset();
}

LifecycleNodeInterface::CallbackReturn
RigidBodyControl::on_configure(const rclcpp_lifecycle::State&)
{
    // Only publishers are initialized here while there is no LifecycleSubscription
    // class implemented in ROS2. Otherwise, messages are received by the subscription
    // even though this node is not active
    pub_cmd   = create_publisher<geometry_msgs::msg::Wrench>("cmd", qos_ctrl);
    pub_error = create_publisher<nav_msgs::msg::Odometry>("error", qos_ctrl);

    // Retrieve parameters
    get_parameter(param_mode);
    //
    get_parameter(param_sdf_file);
    get_parameter(param_sdf_string);
    get_parameter(param_sdf_link);
    //
    params_pid->read();

    // Read control mode
    if (param_mode.as_string().empty() || !(param_mode.as_string() == "position" || param_mode.as_string() == "velocity"))
    {
        RCLCPP_WARN(
            get_logger(),
            "must provide a valid control mode via the parameter %s; mode must be either velocity or position",
            param_mode.get_name().c_str()
        );
        return CallbackReturn::FAILURE;
    }
    is_mode_pos = param_mode.as_string() == "position";

    // Read SDF
    if (param_sdf_link.as_string().empty())
    {
        RCLCPP_WARN(get_logger(), "must provide a link to search for using the %s parameter", param_sdf_link.get_name().c_str());
        return CallbackReturn::FAILURE;
    }
    ::sdf::SDFPtr sdf = scar_ros::sdf::read(param_sdf_file, param_sdf_string, get_logger());
    if (!sdf)
        return CallbackReturn::FAILURE;
    //
    scar_ros::sdf::Link sdf_link(sdf, param_sdf_link.as_string(), get_logger());
    if (!sdf_link.ok())
        return CallbackReturn::FAILURE;
    //
    body.mass = sdf_link.mass();
    body.inertia_matrix = sdf_link.inertia();

    // Configure RigidBody
    body.setMode(scar::rbd::FrameRelation::Source, scar::rbd::FrameRelation::Dynamic);

    // Configure PID
    using namespace scar::controller;
    pid.restart();
    SettingsPID pid_settings;
    if (is_mode_pos)
        pid_settings = SettingsPIDSecondOrder::createFromSpecT(
            params_pid->overshoot(),
            params_pid->settling()
        );
    else
        if (params_pid->use_integral())
            pid_settings = SettingsPIDFirstOrder::createFromSpecT(
                params_pid->overshoot(),
                params_pid->settling()
            );
        else
            pid_settings = SettingsPIDFirstOrder::createFromSpecT(
                params_pid->settling()
            );
    pid.configure(
        pid_settings,
        scar::types::Time::fromSeconds(params_pid->sampling())
    );
    pid.setDerivativeFiltered( params_pid->estimate_derivative() );
    if (params_pid->feedforward())
    {
        pid.setCallbackPostProcessing(
            [this] (scar::controller::Output& out) -> void {
                out += ref_ff;
                out = body.M_pose * out + body.C_pose * body.dot_pose + body.G_pose;
            }
        );
    }
    else
    {
        pid.setCallbackPostProcessing(
            [this] (scar::controller::Output& out) -> void {
                out = body.M_pose * out;
            }
        );
    }
    if (params_pid->use_prefilter())
    {
        scar::controller::SettingsFilter prefilter_settings;
        prefilter_settings.sampling_period = scar::types::Time::fromSeconds(params_pid->sampling());
        pid.enableInputPrefilter(scar::types::Poly::Ones(1), prefilter_settings);
    }

    return CallbackReturn::SUCCESS;
}

LifecycleNodeInterface::CallbackReturn
RigidBodyControl::on_cleanup(const rclcpp_lifecycle::State&)
{
    resetPublishers();
    return CallbackReturn::SUCCESS;
}

LifecycleNodeInterface::CallbackReturn
RigidBodyControl::on_shutdown(const rclcpp_lifecycle::State&)
{
    resetPublishers();
    return CallbackReturn::SUCCESS;
}

LifecycleNodeInterface::CallbackReturn
RigidBodyControl::on_activate(const rclcpp_lifecycle::State&)
{
    using namespace std::placeholders;

    // Activate LifecyclePublisher objects
    pub_cmd->on_activate();
    pub_error->on_activate();

    sub_ref.subscribe(this, "ref", qos_ctrl.get_rmw_qos_profile());
    sub_ff.subscribe(this, "ref_ff", qos_ctrl.get_rmw_qos_profile());
    sub_odom.subscribe(this, "odom", qos_ctrl.get_rmw_qos_profile());
    sub_accel.subscribe(this, "accel", qos_ctrl.get_rmw_qos_profile());
    sync = std::make_shared<SyncPolicy::Sync>(SyncPolicy::Sync(), sub_ref, sub_ff, sub_odom, sub_accel);
    sync->registerCallback(std::bind(&RigidBodyControl::onDataReceived, this, _1, _2, _3, _4));

    has_null = false;

    return CallbackReturn::SUCCESS;
}

LifecycleNodeInterface::CallbackReturn
RigidBodyControl::on_deactivate(const rclcpp_lifecycle::State&)
{
    // Release sync policy
    sync = nullptr;

    // Send zero force/torque
    geometry_msgs::msg::Wrench msg_out;
    pub_cmd->publish(msg_out);

    // Deactivate LifecyclePublisher objects
    pub_cmd->on_deactivate();
    pub_error->on_deactivate();

    // Unsubscribe
    sub_ref.unsubscribe();
    sub_ff.unsubscribe();
    sub_odom.unsubscribe();
    sub_accel.unsubscribe();

    // Restart the PID
    pid.restart();

    return CallbackReturn::SUCCESS;
}

bool RigidBodyControl::update(const rclcpp::Time& time)
{
    if (get_current_state().id() != lifecycle_msgs::msg::State::PRIMARY_STATE_ACTIVE)
        return false;
    sync->getPolicy()->call();
    if (has_null)
        return false;

    body.update(pose, twist, accel);
    body.calc();
    // TODO(jcmonteiro) test if this is working
    decltype(body.dot_pose) ref_dot_pose;
    ref_dot_pose <<
        ref_twist.block<3, 1>(0, 0),
        scar::rbd::jacobianRepRPY(body.rpy) * ref_twist.block<3, 1>(3, 0);
    if (is_mode_pos && !pid.isDerivativeFiltered())
        pid.setErrorDerivative(ref_dot_pose - body.dot_pose);

    if (is_mode_pos)
        pid.update(conversions::convert(time), ref_pose, body.pose);
    else
        pid.update(conversions::convert(time), ref_dot_pose, body.dot_pose);

    auto out = pid.getOutput();
    geometry_msgs::msg::Wrench msg_out;
    msg_out.force.x  = out[0];
    msg_out.force.y  = out[1];
    msg_out.force.z  = out[2];
    msg_out.torque.x = out[3];
    msg_out.torque.y = out[4];
    msg_out.torque.z = out[5];

    // TODO(jcmonteiro) test if this msg is correctly configured
    nav_msgs::msg::Odometry msg_error;
    scar::controller::Input error, error_vel;
    Eigen::Quaterniond error_quat;

    if (is_mode_pos)
    {
        error = ref_pose - body.pose;
        error_vel = pid.getErrorDerivative();
        error_vel.block<3, 1>(3, 0) = scar::rbd::inverseJacobianRepRPY(body.rpy) * error_vel.block<3, 1>(3, 0);
        error_quat = Eigen::AngleAxisd(error[5], Eigen::Vector3d::UnitZ())
                   * Eigen::AngleAxisd(error[4], Eigen::Vector3d::UnitY())
                   * Eigen::AngleAxisd(error[3], Eigen::Vector3d::UnitX());

        msg_error.pose.pose.position.x    = error[0];
        msg_error.pose.pose.position.y    = error[1];
        msg_error.pose.pose.position.z    = error[2];
        msg_error.pose.pose.orientation.x = error_quat.x();
        msg_error.pose.pose.orientation.y = error_quat.y();
        msg_error.pose.pose.orientation.z = error_quat.z();
        msg_error.pose.pose.orientation.w = error_quat.w();
    }
    else
        error_vel = ref_dot_pose - body.dot_pose;

    msg_error.header.stamp = time;
    msg_error.twist.twist.linear.x  = error_vel[0];
    msg_error.twist.twist.linear.y  = error_vel[1];
    msg_error.twist.twist.linear.z  = error_vel[2];
    msg_error.twist.twist.angular.x = error_vel[3];
    msg_error.twist.twist.angular.y = error_vel[4];
    msg_error.twist.twist.angular.z = error_vel[5];

    pub_cmd->publish(msg_out);
    pub_error->publish(msg_error);

    return true;
}
