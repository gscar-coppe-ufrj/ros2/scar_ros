#include "scar_ros/simple_pose_control.hpp"
#include "scar_ros/control/pid_parameters.hpp"
#include "scar_ros/conversions/time.hpp"

#include <lifecycle_msgs/msg/state.hpp>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2/LinearMath/Matrix3x3.h>

#include <scar/controller/SettingsPIDFirstOrder.hpp>


using namespace ::scar_ros;
using rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface;


SimplePoseControl::SimplePoseControl(bool intra_process_comms) :
    LifecycleNode("simple_pose_control",
        rclcpp::NodeOptions().use_intra_process_comms(intra_process_comms)),
    pid(6),
    frame_relation(),
    ref_pose(6), ref_twist(6), pose(6), twist(6),
    qos_ctrl(rclcpp::QoS(rclcpp::KeepLast(5)).durability_volatile().best_effort())
{
    params_pid = std::make_shared<control::PIDParameters<rclcpp_lifecycle::LifecycleNode>>(this);
    params_pid->declare();
}

void SimplePoseControl::onDataReceived(
    const nav_msgs::msg::Odometry::ConstSharedPtr& msg_ref,
    const nav_msgs::msg::Odometry::ConstSharedPtr& msg_odom
)
{
    if (!msg_ref || !msg_odom)
    {
        has_null = true;
        return;
    }
    else
        has_null = false;

    // Reference
    tf2::Quaternion quat(
        msg_ref->pose.pose.orientation.x,
        msg_ref->pose.pose.orientation.y,
        msg_ref->pose.pose.orientation.z,
        msg_ref->pose.pose.orientation.w
    );
    double roll, pitch, yaw;
    tf2::Matrix3x3(quat).getEulerYPR(yaw, pitch, roll);
    ref_pose << msg_ref->pose.pose.position.x, msg_ref->pose.pose.position.y, msg_ref->pose.pose.position.z,
            roll, pitch, yaw;
    //
    ref_twist << msg_ref->twist.twist.linear.x, msg_ref->twist.twist.linear.y, msg_ref->twist.twist.linear.z,
                 msg_ref->twist.twist.angular.x, msg_ref->twist.twist.angular.y, msg_ref->twist.twist.angular.z;

    // Odometry
    quat = tf2::Quaternion(
        msg_odom->pose.pose.orientation.x,
        msg_odom->pose.pose.orientation.y,
        msg_odom->pose.pose.orientation.z,
        msg_odom->pose.pose.orientation.w
    );
    tf2::Matrix3x3(quat).getEulerYPR(yaw, pitch, roll);
    pose << msg_odom->pose.pose.position.x, msg_odom->pose.pose.position.y, msg_odom->pose.pose.position.z,
            roll, pitch, yaw;
    //
    twist << msg_odom->twist.twist.linear.x, msg_odom->twist.twist.linear.y, msg_odom->twist.twist.linear.z,
             msg_odom->twist.twist.angular.x, msg_odom->twist.twist.angular.y, msg_odom->twist.twist.angular.z;
}

void SimplePoseControl::resetPublishers()
{
    pub_cmd.reset();
    pub_error.reset();
    pub_dot_cmd.reset();
}

LifecycleNodeInterface::CallbackReturn
SimplePoseControl::on_configure(const rclcpp_lifecycle::State&)
{
    // Only publishers are initialized here while there is no LifecycleSubscription
    // class implemented in ROS2. Otherwise, messages are received by the subscription
    // even though this node is not active
    pub_cmd     = create_publisher<nav_msgs::msg::Odometry>("cmd", qos_ctrl);
    pub_error   = create_publisher<nav_msgs::msg::Odometry>("error", qos_ctrl);
    pub_dot_cmd = create_publisher<geometry_msgs::msg::AccelStamped>("dot_cmd", qos_ctrl);

    // Retrieve parameters
    params_pid->read();

    // Configure FrameRelation
    frame_relation.setMode(scar::rbd::FrameRelation::Source, scar::rbd::FrameRelation::Kinematic);

    // Configure PID
    using namespace scar::controller;
    pid.restart();
    SettingsPID pid_settings;
    if (params_pid->use_integral())
        pid_settings = SettingsPIDFirstOrder::createFromSpecT(
            params_pid->overshoot(),
            params_pid->settling()
        );
    else
        pid_settings = SettingsPIDFirstOrder::createFromSpecT(
            params_pid->settling()
        );
    pid.configure(
        pid_settings,
        scar::types::Time::fromSeconds(params_pid->sampling())
    );
    if (params_pid->feedforward())
    {
        pid.setCallbackPostProcessing(
            [this] (scar::controller::Output& out) -> void {
                out += ref_twist;
                out.block<3, 1>(3, 0) = scar::rbd::inverseJacobianRepRPY(frame_relation.rpy) * out.block<3, 1>(3, 0);
            }
        );
    }
    else
    {
        pid.setCallbackPostProcessing(
            [this] (scar::controller::Output& out) -> void {
                out.block<3, 1>(3, 0) = scar::rbd::inverseJacobianRepRPY(frame_relation.rpy) * out.block<3, 1>(3, 0);
            }
        );
    }

    return CallbackReturn::SUCCESS;
}

LifecycleNodeInterface::CallbackReturn
SimplePoseControl::on_cleanup(const rclcpp_lifecycle::State&)
{
    resetPublishers();
    return CallbackReturn::SUCCESS;
}

LifecycleNodeInterface::CallbackReturn
SimplePoseControl::on_shutdown(const rclcpp_lifecycle::State&)
{
    resetPublishers();
    return CallbackReturn::SUCCESS;
}

LifecycleNodeInterface::CallbackReturn
SimplePoseControl::on_activate(const rclcpp_lifecycle::State&)
{
    using namespace std::placeholders;

    // Activate LifecyclePublisher objects
    pub_cmd->on_activate();
    pub_error->on_activate();
    pub_dot_cmd->on_activate();

    sub_ref.subscribe(this, "ref", qos_ctrl.get_rmw_qos_profile());
    sub_odom.subscribe(this, "odom", qos_ctrl.get_rmw_qos_profile());
    sync = std::make_shared<SyncPolicy::Sync>(SyncPolicy::Sync(), sub_ref, sub_odom);
    sync->registerCallback(std::bind(&SimplePoseControl::onDataReceived, this, _1, _2));

    has_null = false;

    return CallbackReturn::SUCCESS;
}

LifecycleNodeInterface::CallbackReturn
SimplePoseControl::on_deactivate(const rclcpp_lifecycle::State&)
{
    // Release sync policy
    sync = nullptr;

    // Deactivate LifecyclePublisher objects
    pub_cmd->on_deactivate();
    pub_error->on_deactivate();
    pub_dot_cmd->on_deactivate();

    // Unsubscribe
    sub_ref.unsubscribe();
    sub_odom.unsubscribe();

    // Restart the PID
    pid.restart();

    return CallbackReturn::SUCCESS;
}

bool SimplePoseControl::update(const rclcpp::Time& time)
{
    if (get_current_state().id() != lifecycle_msgs::msg::State::PRIMARY_STATE_ACTIVE)
        return false;
    sync->getPolicy()->call();
    if (has_null)
        return false;

    frame_relation.update(pose, twist);
    ref_twist <<
        ref_twist.block<3, 1>(0, 0),
        scar::rbd::jacobianRepRPY(frame_relation.rpy) * ref_twist.block<3, 1>(3, 0);

    pid.update(conversions::convert(time), ref_pose, frame_relation.pose);

    nav_msgs::msg::Odometry msg_cmd;

    const auto& out = pid.getOutput();

    msg_cmd.header.stamp = time;
    msg_cmd.twist.twist.linear.x = out(0);
    msg_cmd.twist.twist.linear.y = out(1);
    msg_cmd.twist.twist.linear.z = out(2);
    msg_cmd.twist.twist.angular.x = out(3);
    msg_cmd.twist.twist.angular.y = out(4);
    msg_cmd.twist.twist.angular.z = out(5);

    scar::controller::Input error;
    Eigen::Quaterniond error_quat;
    error = ref_pose - frame_relation.pose;
    error_quat = Eigen::AngleAxisd(error[5], Eigen::Vector3d::UnitZ())
                * Eigen::AngleAxisd(error[4], Eigen::Vector3d::UnitY())
                * Eigen::AngleAxisd(error[3], Eigen::Vector3d::UnitX());
    nav_msgs::msg::Odometry msg_error;
    msg_error.header.stamp = time;
    msg_error.pose.pose.position.x    = error[0];
    msg_error.pose.pose.position.y    = error[1];
    msg_error.pose.pose.position.z    = error[2];
    msg_error.pose.pose.orientation.x = error_quat.x();
    msg_error.pose.pose.orientation.y = error_quat.y();
    msg_error.pose.pose.orientation.z = error_quat.z();
    msg_error.pose.pose.orientation.w = error_quat.w();

    pub_cmd->publish(msg_cmd);
    pub_error->publish(msg_error);

    return true;
}
