#include "scar_ros/trajectory_generator.hpp"

#include <scar/rbd/FrameRelation.hpp>


using namespace ::scar_ros;


TrajectoryGenerator::TrajectoryGenerator(bool intra_process_comms):
    LifecycleNode("trajectory_node", rclcpp::NodeOptions().use_intra_process_comms(intra_process_comms)),
    traj(nullptr),
    qos_trajectory(rclcpp::QoS(rclcpp::KeepLast(5)).durability_volatile().best_effort())
{

    param_traj_type = rclcpp::Parameter("type", "empty");

    param_amplitude = rclcpp::Parameter("sine/amplitude", std::vector<double>({0.0}));
    param_frequency = rclcpp::Parameter("sine/frequency", std::vector<double>({0.0}));
    param_phase     = rclcpp::Parameter("sine/phase", std::vector<double>({0.0}));
    param_offset    = rclcpp::Parameter("sine/offset", std::vector<double>({0.0}));
    param_order     = rclcpp::Parameter("sine/order", 0);

    param_initial_data_dimensions = rclcpp::Parameter("two_point/initial_data_dimensions", std::vector<int>({0, 0}));
    param_initial_time_sec        = rclcpp::Parameter("two_point/initial_time_sec", 0.0);
    param_initial_data            = rclcpp::Parameter("two_point/initial_data", std::vector<double>({0.0}));
    param_final_data_dimensions   = rclcpp::Parameter("two_point/final_data_dimensions", std::vector<int>({0, 0}));
    param_final_time_sec          = rclcpp::Parameter("two_point/final_time_sec", 0.0);
    param_final_data              = rclcpp::Parameter("two_point/final_data", std::vector<double>({0.0}));
    param_max_vel                 = rclcpp::Parameter("two_point/max_vel", std::vector<double>({0.0}));


    // Declare parameters
    this->declare_parameter(param_traj_type.get_name(), param_traj_type.get_parameter_value());

    // Sine Parameters
    this->declare_parameter(param_amplitude.get_name(), param_amplitude.get_parameter_value());
    this->declare_parameter(param_frequency.get_name(), param_frequency.get_parameter_value());
    this->declare_parameter(param_offset.get_name(),    param_offset.get_parameter_value());
    this->declare_parameter(param_phase.get_name(),     param_phase.get_parameter_value());
    this->declare_parameter(param_order.get_name(),     param_order.get_parameter_value());

    // Two Point Parameters
    this->declare_parameter(param_initial_data_dimensions.get_name(), param_initial_data_dimensions.get_parameter_value());
    this->declare_parameter(param_final_data_dimensions.get_name(),   param_final_data_dimensions.get_parameter_value());
    this->declare_parameter(param_initial_time_sec.get_name(),        param_initial_time_sec.get_parameter_value());
    this->declare_parameter(param_final_time_sec.get_name(),          param_final_time_sec.get_parameter_value());
    this->declare_parameter(param_initial_data.get_name(),            param_initial_data.get_parameter_value());
    this->declare_parameter(param_final_data.get_name(),              param_final_data.get_parameter_value());
    this->declare_parameter(param_max_vel.get_name(),                 param_max_vel.get_parameter_value());

    get_parameter(param_traj_type.get_name(), param_traj_type);
    std::cout << param_traj_type.get_name() << "  -  " << param_traj_type.as_string() << std::endl;

}

TrajectoryGenerator::~TrajectoryGenerator()
{
}

void TrajectoryGenerator::resetPublishers()
{
    pub_accel_cov.reset();
    pub_odom.reset();
}

bool TrajectoryGenerator::update(const rclcpp::Time& ts)
{
    // Time messages
    msg_accel.header.stamp = ts;
    msg_odom.header.stamp = ts;

    auto scar_time = scar_ros::conversions::convert(ts);

    //convert RPY to Quaternion
    Eigen::Quaterniond q;
    scar::types::Waypoint wp = traj->get(scar_time);
    const scar::types::Waypoint::Data& data = wp.getData();
    auto rpy = data.block<3,1>(3, 0);

    q = Eigen::AngleAxisd(rpy(2), Eigen::Vector3d::UnitZ()) *
        Eigen::AngleAxisd(rpy(1), Eigen::Vector3d::UnitY()) *
        Eigen::AngleAxisd(rpy(0), Eigen::Vector3d::UnitX());

    // Pose message
    msg_odom.pose.pose.position.x    = data(0,0);
    msg_odom.pose.pose.position.y    = data(1,0);
    msg_odom.pose.pose.position.z    = data(2,0);
    msg_odom.pose.pose.orientation.w = q.w();
    msg_odom.pose.pose.orientation.x = q.x();
    msg_odom.pose.pose.orientation.y = q.y();
    msg_odom.pose.pose.orientation.z = q.z();

    // Twist message
    msg_odom.twist.twist.linear.x  = data(0,1);
    msg_odom.twist.twist.linear.y  = data(1,1);
    msg_odom.twist.twist.linear.z  = data(2,1);
    auto inv_Jrep = scar::rbd::inverseJacobianRepRPY(rpy);
    auto dot_rpy = data.block<3,1>(3, 1);
    auto ang_vel = inv_Jrep * dot_rpy;
    msg_odom.twist.twist.angular.x = ang_vel(0);
    msg_odom.twist.twist.angular.y = ang_vel(1);
    msg_odom.twist.twist.angular.z = ang_vel(2);

    // Accel message
    msg_accel.accel.angular.x = data(0,2);
    msg_accel.accel.angular.y = data(1,2);
    msg_accel.accel.angular.z = data(2,2);
    auto dot_inv_Jrep = scar::rbd::dotInverseJacobianRepRPY(rpy, dot_rpy);
    auto ddot_rpy = data.block<3,1>(3, 2);
    auto ang_acc = dot_inv_Jrep * dot_rpy + inv_Jrep * ddot_rpy;
    msg_accel.accel.linear.x  = ang_acc(0);
    msg_accel.accel.linear.y  = ang_acc(1);
    msg_accel.accel.linear.z  = ang_acc(2);

    pub_odom->publish(msg_odom);
    pub_accel_cov->publish(msg_accel);

    return true;
}

rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn
TrajectoryGenerator::on_configure(const rclcpp_lifecycle::State&)
{
    pub_accel_cov = this->create_publisher<geometry_msgs::msg::AccelStamped>("accel", qos_trajectory);
    pub_odom      = this->create_publisher<nav_msgs::msg::Odometry>("odom", qos_trajectory);

    msg_odom  = nav_msgs::msg::Odometry();
    msg_accel = geometry_msgs::msg::AccelStamped();

    get_parameter(param_traj_type.get_name(), param_traj_type);
    if (param_traj_type.as_string() == "sine")
    {
        //Retrieve parameters
        get_parameter(param_amplitude.get_name(), param_amplitude);
        get_parameter(param_frequency.get_name(), param_frequency);
        get_parameter(param_phase.get_name(),     param_phase);
        get_parameter(param_offset.get_name(),    param_offset);
        get_parameter(param_order.get_name(),     param_order);

        size_t cols = 4;
        size_t rows = 6;

        Eigen::MatrixXd config(rows, cols);
        int col_index = 0;
        std::vector<double> params[] = {
            param_amplitude.as_double_array(),
            param_frequency.as_double_array(),
            param_phase.as_double_array(),
            param_offset.as_double_array()
        };

        // Check if parameters have the proper size
        for(size_t i = 0; i < cols; ++i)
        {
            if(params[i].size() != rows)
            {
                RCLCPP_ERROR(
                    get_logger(),
                    "Parameter with invalid size"
                );
                return rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn::FAILURE;
            }
        }

        // Convert frequency from Hz to rad/s; the unit expected by scar::trajectory::TrajectoryBase
        for (auto & freq : params[1])
            freq = freq * M_PI * 2;
        for (auto & parameter_list : params)
        {
            int row_index = 0;
            for (auto& value : parameter_list)
            {
                config(row_index, col_index) = value;
                ++row_index;
            }
            ++col_index;
        }
        int order = param_order.as_int();
        traj = std::make_shared<scar::trajectory::TrajectorySine>(config, order);
    }
    else if (param_traj_type.as_string() == "two_point")
    {
        get_parameter(param_initial_data_dimensions.get_name(), param_initial_data_dimensions);
        get_parameter(param_final_data_dimensions.get_name(), param_final_data_dimensions);
        get_parameter(param_initial_data.get_name(), param_initial_data);
        get_parameter(param_final_data.get_name(), param_final_data);
        get_parameter(param_max_vel.get_name(), param_max_vel);
        get_parameter(param_initial_time_sec.get_name(), param_initial_time_sec);
        get_parameter(param_final_time_sec.get_name(), param_final_time_sec);

        if(param_final_data_dimensions.as_integer_array().size() != 2 || param_initial_data_dimensions.as_integer_array().size() != 2)
        {
            RCLCPP_ERROR(
                    get_logger(),
                    "Paremeter - Waypoints dimensions - with invalid size"
            );
            return rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn::FAILURE;
        }
        auto rows = param_initial_data_dimensions.as_integer_array()[0];
        auto cols = param_initial_data_dimensions.as_integer_array()[1];

        if (rows != param_initial_data_dimensions.as_integer_array()[0] || cols != param_initial_data_dimensions.as_integer_array()[1])
        {
            RCLCPP_ERROR(
                    get_logger(),
                    "Paremeter - Waypoints dimensions - size differs"
            );
            return rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn::FAILURE;
        }

        Eigen::MatrixXd initial_data(rows, cols);
        Eigen::MatrixXd final_data(rows, cols);
        scar::types::Waypoint::Data v_max(rows, 1);
        auto initial_double_array = param_initial_data.as_double_array();
        auto final_double_array = param_final_data.as_double_array();
        auto v_max_array = param_max_vel.as_double_array();

        int index = 0;
        for(int i = 0; i < rows; ++i)
        {
            for(int j = 0; j < cols; ++j)
            {
                initial_data(i,j) = initial_double_array[index];
                final_data(i,j) = final_double_array[index];
                ++index;
            }
            v_max(i) = v_max_array[i];
        }

        scar::types::Time initial_time = scar::types::Time::fromSeconds(param_initial_time_sec.as_double());
        scar::types::Time final_time = scar::types::Time::fromSeconds(param_final_time_sec.as_double());
        scar::types::Waypoint initial_waypoint = scar::types::Waypoint(initial_data, initial_time);
        scar::types::Waypoint final_waypoint = scar::types::Waypoint(final_data, final_time);

        scar::trajectory::TrajectoryWaypoints::Waypoints ways = {initial_waypoint, final_waypoint};
        traj = std::make_shared<scar::trajectory::TrajectoryTwoPoint>(ways, v_max);

    }
    else
    {
        RCLCPP_ERROR(
            get_logger(),
            "failed to configure: could not found a valid trajectory type in parameter %s",
            param_traj_type.get_name().c_str()
        );
        return rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn::FAILURE;
    }

    return rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn::SUCCESS;
}

rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn
TrajectoryGenerator::on_activate(const rclcpp_lifecycle::State&)
{
    pub_accel_cov->on_activate();
    pub_odom->on_activate();
    return rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn::SUCCESS;
}

rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn
TrajectoryGenerator::on_cleanup(const rclcpp_lifecycle::State&)
{
    resetPublishers();
    return
    rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn::SUCCESS;
}

rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn
TrajectoryGenerator::on_deactivate(const rclcpp_lifecycle::State&)
{
    pub_odom->on_deactivate();
    pub_accel_cov->on_deactivate();
    return
    rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn::SUCCESS;
}

rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn
TrajectoryGenerator::on_shutdown(const rclcpp_lifecycle::State&)
{
    resetPublishers();
    return rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn::SUCCESS;
}
